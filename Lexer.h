//
// Created by sc18gg on 2/8/20.
//

#ifndef JACK_COMPILER_LEXER_H
#define JACK_COMPILER_LEXER_H

#include <iostream>
#include <fstream>
#include <cstdlib>

#include <vector>
#include <string>
#include <cstring>

#include "Token.h"

using namespace std;

/**
 * This files represents a lexer class. It reads a file and tokenises it for the later use of Parser.
 */

class Lexer {

public:

    Lexer()= default;;

    void ReadInput(const string& filename);

    void TokenizeAll();

    Token* GetNextToken();

    Token* PeekNextToken();

    Token* PeekNextTokenPlus(int extra);

    vector<Token*> &getTokenVector() {
        return Tokens;
    }

    void setTokenVector(vector<Token*> &new_vector) {
        Tokens = new_vector;
    }

    bool TokenEmpty();

    int GetListSize() {
        return inputList.size();
    }

    int getTokensSize() {
        return Tokens.size();
    }

    int GetTokenSize() {
        return Tokens.size();
    }

    char listAt(int position) {
        return inputList.at(position);
    }
private:

    vector<char> inputList;
    vector<Token*> Tokens;

};

#endif //JACK_COMPILER_LEXER_H
