//
// Created by sc18gg on 2/29/20.
//

#include "Parser.h"

/**
 * This file represents a recursive descent parser, which will be used by the jack compiler.
 * Every function is a production which is derived by the grammar rules of the jack language.
 * Every function will contain a description with the corresponding production applied.
 * Terminals are represented in lower camel case and non-terminals are represented in upper camel case. (in comments)
 */

// Initialises the program table.
vector<vector<vector<Symbol*>>> Parser::currentProgramState = {};

// This function resolves an expression given and populates VM stack with corresponding VM commands.
void Parser::resolveExpression(vector<Token*> expression) {

    // Expression is a constant.
    if (expression.size() == 1 && expression.at(0)->getType() == Token::TokenTypes::number) {
        vmf << "push constant" << " " << expression.at(0)->getLexeme() << endl;
    }
    else if (expression.size() == 1 && expression.at(0)->getType() == Token::TokenTypes::keyword && expression.at(0)->getLexeme() == "this") {
        vmf << "push pointer 0" << endl;
    }
        // Expression is a boolean constant.
    else if (expression.size() == 1 && expression.at(0)->getType() == Token::TokenTypes::keyword &&
        (expression.at(0)->getLexeme() == "true" || expression.at(0)->getLexeme() == "false")) {
        if (expression.at(0)->getLexeme() == "true") {
            vmf << "push constant 0" << endl;
            vmf << "not" << endl;
        }
        else if (expression.at(0)->getLexeme() == "false") {
            vmf << "push constant 0" << endl;
        }
    }
    // Expression is variable.
    else if (expression.size() == 1 && expression.at(0)->getType() == Token::TokenTypes::id) {
        Symbol *variable = findSymbol(expression.at(0)->getLexeme());

        if (variable->getKind() == "var") {
            vmf << "push local " << variable->getPosition() << endl;
        }
        else if (variable->getKind() == "field") {
            vmf << "push this " << variable->getPosition() << endl;
        }
        // Argument variable.
        else if (variable->getKind() == "argument") {
            // If the current function is method, follow the symbol table positions for arguments.
            if (determineFunctionKind(determineFunctionPosition()) == "method") {
                vmf << "push " << variable->getKind() << " " << variable->getPosition() << endl;
            }
            // Else decrease the position of arguments by 1. (Because 'this' takes up the first position).
            else {
                vmf << "push " << variable->getKind() << " " << variable->getPosition() - 1 << endl;
            }
        }
        // Static variable.
        else  {
            vmf << "push " << variable->getKind() << " " << variable->getPosition() << endl;
        }

    }
    // Expression is a string literal.
    else if (expression.size() == 1 && expression.at(0)->getType() == Token::TokenTypes::string_literal) {
        int stringSize = expression.at(0)->getLexeme().size();
        string str = expression.at(0)->getLexeme();

        vmf << "push constant " << stringSize << endl;

        vmf << "call String.new 1" << endl;

        for (int i = 0; i < stringSize; ++i) {
            // Pushing ascii codes of every character.
            vmf << "push constant " << int(str.at(i)) << endl;

            // Append to the resulting string.
            vmf << "call String.appendChar 2" << endl;
        }
    }
        // constant followed by expression.
    else if (expression.size() > 1 && expression.at(0)->getType() == Token::TokenTypes::number) {
        vmf << "push constant" << " " << expression.at(0)->getLexeme() << endl;

        vector<Token*> exp1;
        for (int i = 1; i < expression.size(); ++i) {
            exp1.push_back(expression.at(i));
        }
        resolveExpression(exp1);
    }
    // (expression)
    else if (expression.size() > 1 && expression.at(0)->getType() == Token::TokenTypes::symbol &&
             expression.at(0)->getLexeme() == "(") {

        vector<Token*> exp1;
        vector<Token*> exp2;

        int position = 0;
        int bracketsCount = 0;

        // Saving the expresion inside brackets until the corersponding closing bracket.
        while (true) {

            if (expression.at(position)->getLexeme() == "(") {
                ++bracketsCount;
                if (bracketsCount != 1) {
                    exp1.push_back(expression.at(position));
                }
            }
            else if (expression.at(position)->getLexeme() == ")") {
                --bracketsCount;
                if (bracketsCount == 0) {
                    // Finished copying.
                    break;
                }
                exp1.push_back(expression.at(position));
            }
            else {
                exp1.push_back(expression.at(position));
            }

            ++position;
            if (position >= expression.size()) {
                break;
            }
        }

        ++position;
        while (position < expression.size()) {
            exp2.push_back(expression.at(position));
            ++position;
        }

        // First resolve the inside of the brackets and then the outside.
        resolveExpression(exp1);
        resolveExpression(exp2);
    }
    // variable followed by expression.
    else if (expression.size() > 1 && expression.at(0)->getType() == Token::TokenTypes::id &&
             expression.at(1)->getLexeme() != "(") {

        Symbol *identifier = findSymbol(expression.at(0)->getLexeme());
        vector<Token*> exp1;

        bool classVar = false;

        // Just variable which belongs to the current class.
        if (expression.at(1)->getLexeme() != ".") {
            // var[exp]
            if (2 < expression.size() && expression.at(1)->getLexeme() == "[") {
                vmf << "push that 0" << endl;
            }
            else {
                if (identifier->getKind() == "var") {
                    vmf << "push local " << identifier->getPosition() << endl;
                }
                else if (identifier->getKind() == "field") {
                    vmf << "push this " << identifier->getPosition() << endl;
                }
                // Argument variable.
                else if (identifier->getKind() == "argument") {
                    // If the current function is method, follow the symbol table positions for arguments.
                    if (determineFunctionKind(determineFunctionPosition()) == "method") {
                        vmf << "push " << identifier->getKind() << " " << identifier->getPosition() << endl;
                    }
                    // Else decrease the position of arguments by 1. (Because 'this' takes up the first position).
                    else {
                        vmf << "push " << identifier->getKind() << " " << identifier->getPosition() - 1 << endl;
                    }
                }
                // Static variable.
                else  {
                    vmf << "push " << identifier->getKind() << " " << identifier->getPosition() << endl;
                }
            }
            for (int i = 1; i < expression.size(); ++i) {
                exp1.push_back(expression.at(i));
            }
            resolveExpression(exp1);
        }
        // Variable or function which belongs to another class.
        else {
            if (2 < expression.size() && expression.at(2)->getType() == Token::TokenTypes::id) {

                Symbol *nextIdentifier;

                vector<Token*> restExp;

                // class.function
                if (3 < expression.size() && expression.at(3)->getLexeme() == "(") {
                    // If class is the Class Name
                    if (identifier->getKind() == "class") {
                        nextIdentifier = findFunctionInClass(expression.at(2)->getLexeme(), findClassPosition(identifier->getName()));

                        int bracketsCount = 0;
                        int position = 3; // Starting from the bracket.

                        // Pushing arguments.

                        vector<vector<Token*>> args;

                        args.emplace_back();

                        int j = 0;

                        while (true) {

                            if (expression.at(position)->getLexeme() == "(") {
                                ++bracketsCount;
                                if (bracketsCount != 1) {
                                    args.at(j).push_back(expression.at(position));
                                }
                            }
                            else if (expression.at(position)->getLexeme() == ")") {
                                --bracketsCount;
                                if (bracketsCount == 0) {
                                    // Finished copying.
                                    break;
                                }
                                args.at(j).push_back(expression.at(position));
                            }
                            else if (expression.at(position)->getLexeme() == ",") {
                                args.emplace_back();
                                ++j;
                            }
                            else {
                                args.at(j).push_back(expression.at(position));
                            }

                            ++position;
                            if (position >= expression.size()) {
                                break;
                            }
                        }

                        for (int k = 0; k < args.size(); ++k) {
                            resolveExpression(args.at(k));
                        }

                        int numArgs;

                        if (args.empty() || (args.size() == 1 && args.at(0).empty())) {
                            numArgs = 0;
                        }
                        else {
                            numArgs = args.size();
                        }

                        // Calls without passing the object to the function.
                        vmf << "call " << identifier->getName() << "." << nextIdentifier->getName() << " " << numArgs << endl;

                        // Start from the next token in the expression;s
                        ++position;
                        // Resolve the rest of the expression.
                        while (position < expression.size()) {
                            restExp.push_back(expression.at(position));
                            ++position;
                        }

                        resolveExpression(restExp);
                    }
                    // class is a variable with type of another class.
                    else if (identifier->getKind() == "var" || identifier->getKind() == "argument" ||
                             identifier->getKind() == "static" || identifier->getKind() == "field") {

                        string segmentAccess;

                        if (identifier->getKind() == "var") {
                            segmentAccess = "push local " + to_string(identifier->getPosition());
                        }
                        else if (identifier->getKind() == "field") {
                            segmentAccess = "push this " + to_string(identifier->getPosition());
                        }
                            // Static or argument variable.
                        else {
                            segmentAccess = "push " + identifier->getKind() + " " + to_string(identifier->getPosition());
                        }

                        classVar = true;

                        // Pushing the object.
                        vmf << segmentAccess << endl;

                        string class_name = identifier->getType();
                        int bracketsCount = 0;
                        int position = 3; // Starting from the bracket.

                        // Pushing arguments.

                        vector<vector<Token*>> args;

                        args.emplace_back();

                        int j = 0;

                        while (true) {

                            if (expression.at(position)->getLexeme() == "(") {
                                ++bracketsCount;
                                if (bracketsCount != 1) {
                                    args.at(j).push_back(expression.at(position));
                                }
                            }
                            else if (expression.at(position)->getLexeme() == ")") {
                                --bracketsCount;
                                if (bracketsCount == 0) {
                                    // Finished copying.
                                    break;
                                }
                                args.at(j).push_back(expression.at(position));
                            }
                            else if (expression.at(position)->getLexeme() == ",") {
                                args.emplace_back();
                                ++j;
                            }
                            else {
                                args.at(j).push_back(expression.at(position));
                            }

                            ++position;
                            if (position >= expression.size()) {
                                break;
                            }
                        }

                        for (int k = 0; k < args.size(); ++k) {
                            resolveExpression(args.at(k));
                        }

                        nextIdentifier = findFunctionInClass(expression.at(2)->getLexeme(), findClassPosition(class_name));

                        int numArgs;

                        if (args.empty() || (args.size() == 1 && args.at(0).empty())) {
                            if (classVar) {
                                numArgs = 1;
                            }
                            else {
                                numArgs = 0;
                            }
                        }
                        else {
                            numArgs = args.size();
                        }

                        // Calling the corresponding function with the required amount of arguments.
                        vmf << "call " << class_name << "." << nextIdentifier->getName() << " " << numArgs << endl;

                        // Start from the next token in the expression;s
                        ++position;
                        // Resolve the rest of the expression.
                        while (position < expression.size()) {
                            restExp.push_back(expression.at(position));
                            ++position;
                        }

                        resolveExpression(restExp);
                    }
                }
                    // class.variable
                else {
                    // If class is the Class Name
                    if (identifier->getKind() == "class") {
                        nextIdentifier = findSymbolInClass(expression.at(2)->getLexeme(), findClassPosition(identifier->getName()));

                        vmf << "push " << identifier->getName() << "." << nextIdentifier->getName() << endl;

                        // Start from the next token in the expression;
                        int position = 3;
                        // Resolve the rest of the expression.
                        while (position < expression.size()) {
                            restExp.push_back(expression.at(position));
                            ++position;
                        }

                        resolveExpression(restExp);
                    }
                        // class is a variable with type of another class.
                    else if (identifier->getKind() == "var" || identifier->getKind() == "argument" ||
                             identifier->getKind() == "static" || identifier->getKind() == "field") {

                        string class_name = identifier->getType();

                        nextIdentifier = findSymbolInClass(expression.at(2)->getLexeme(), findClassPosition(class_name));

                        int positionIdentifier = nextIdentifier->getPosition();

                        if (identifier->getKind() == "var") {
                            vmf << "push local " << identifier->getPosition() << endl;
                        }
                        else if (identifier->getKind() == "field") {
                            vmf << "push this " << identifier->getPosition() << endl;
                        }
                        // Argument variable.
                        else if (identifier->getKind() == "argument") {
                            // If the current function is method, follow the symbol table positions for arguments.
                            if (determineFunctionKind(determineFunctionPosition()) == "method") {
                                vmf << "push " << identifier->getKind() << " " << identifier->getPosition() << endl;
                            }
                            // Else decrease the position of arguments by 1. (Because 'this' takes up the first position).
                            else {
                                vmf << "push " << identifier->getKind() << " " << identifier->getPosition() - 1 << endl;
                            }
                        }
                        // Static variable.
                        else  {
                            vmf << "push " << identifier->getKind() << " " << identifier->getPosition() << endl;
                        }

                        // Pop class to 'that' segment;
                        vmf << "pop pointer 1" << endl;

                        // Popping the corresponding field of the class.
                        vmf << "pop that " << positionIdentifier << endl;

                        // Start from the next token in the expression;
                        int position = 3;
                        // Resolve the rest of the expression.
                        while (position < expression.size()) {
                            restExp.push_back(expression.at(position));
                            ++position;
                        }

                        resolveExpression(restExp);
                    }
                }
            }
        }
    }

    // Function(exp1, exp2, ... , expn) followed by expression.
    else if (expression.size() > 1 && expression.at(0)->getType() == Token::TokenTypes::id &&
             expression.at(1)->getLexeme() == "(") {

        string function_name = expression.at(0)->getLexeme();

        vector<vector<Token*>> exps;

        vector<Token*> exp2;

        exps.emplace_back();

        // Pushing the current object.
        vmf << "push pointer 0" << endl;

        int i = 2;
        int argNumber = 0;

        // Already seen one.
        int numBrack = 1;

        // Save the expressionList
        while (numBrack != 0 && i < expression.size()) {
            if (expression.at(i)->getLexeme() == ",") {
                exps.emplace_back();
                ++argNumber;
            }
            else if (expression.at(i)->getLexeme() == "(") {
                ++numBrack;
                exps.at(argNumber).push_back(expression.at(i));
            }
            else if (expression.at(i)->getLexeme() == ")") {
                --numBrack;
                if (numBrack != 0) {
                    exps.at(argNumber).push_back(expression.at(i));
                }
            }
            else {
                exps.at(argNumber).push_back(expression.at(i));
            }
            ++i;
        }

        for (int k = 0; k < exps.size(); ++k) {
            resolveExpression(exps.at(k));
        }

        int numArgs;

        if (exps.empty() || (exps.size() == 1 && exps.at(0).empty())) {
            numArgs = 0;
        }
        else {
            numArgs = exps.size();
        }

        vmf << "call " << className << "." << function_name << " " << numArgs + 1 << endl;

        // Resolve the rest of the expression.
        while (i < expression.size()) {
            exp2.push_back(expression.at(i));
            ++i;
        }

        if (!exp2.empty()) {
            resolveExpression(exp2);
        }
    }

    // Operator followed by expression.
    else if (expression.size() > 1 && checkIfOperator(expression.at(0)->getLexeme())) {
        vector<Token*> exp1;
        string op = expression.at(0)->getLexeme();
        Symbol *varSymbol = nullptr;
        bool isConstant = false;

        if ((op == "*" || op == "/") && expression.at(1)->getLexeme() != "(") {
            if (expression.at(1)->getType() == Token::TokenTypes::number) {
                isConstant = true;
                for (int i = 2; i < expression.size(); ++i) {
                    exp1.push_back(expression.at(i));
                }
            }
            else if (expression.at(1)->getType() == Token::TokenTypes::id) {
                varSymbol = findSymbol(expression.at(1)->getLexeme());

                if (varSymbol->getKind() == "var" || varSymbol->getKind() == "field" ||
                    varSymbol->getKind() == "static" || varSymbol->getKind() == "argument") {

                    for (int i = 2; i < expression.size(); ++i) {
                        exp1.push_back(expression.at(i));
                    }
                }
            }
        }
        else {
            for (int i = 1; i < expression.size(); ++i) {
                exp1.push_back(expression.at(i));
            }
        }

        // Changes precedence.
        if ((op == "*" || op == "/") && expression.at(1)->getLexeme() != "(") {

            // Resolving * or / followed by something that is not a bracket;
            if (varSymbol != nullptr) {
                // Array symbol.
                if (expression.at(1)->getType() == Token::TokenTypes::id && 2 < expression.size() && expression.at(2)->getLexeme() == "[") {
                    // Already resolved, so push its result.
                    vmf << "push that 0" << endl;
                }
                else {
                    if (varSymbol->getKind() == "var") {
                        vmf << "push local " << varSymbol->getPosition() << endl;
                    } else if (varSymbol->getKind() == "field") {
                        vmf << "push this " << varSymbol->getPosition() << endl;
                    }
                    // Argument variable.
                    else if (varSymbol->getKind() == "argument") {
                        // If the current function is method, follow the symbol table positions for arguments.
                        if (determineFunctionKind(determineFunctionPosition()) == "method") {
                            vmf << "push " << varSymbol->getKind() << " " << varSymbol->getPosition() << endl;
                        }
                        // Else decrease the position of arguments by 1. (Because 'this' takes up the first position).
                        else {
                            vmf << "push " << varSymbol->getKind() << " " << varSymbol->getPosition() - 1 << endl;
                        }
                    }
                    // Static variable.
                    else  {
                        vmf << "push " << varSymbol->getKind() << " " << varSymbol->getPosition() << endl;
                    }
                }
            }
            else if (isConstant) {
                vmf << "push constant" << " " << expression.at(1)->getLexeme() << endl;
            }

            outputOperator(op);
            resolveExpression(exp1);
        }
        else {
            resolveExpression(exp1);
            outputOperator(op);
        }
    }
        // Expression operator expression.
    else if (expression.size() > 1 && !checkIfOperator(expression.at(0)->getLexeme())) {
        int operatorPos;
        for (int i = 0; i < expression.size(); ++i) {
            if (checkIfOperator(expression.at(i)->getLexeme())) {
                vector<Token*> exp1;
                vector<Token*> exp2;

                operatorPos = i;

                for (int j = 0; j < i; ++j) {
                    exp1.push_back(expression.at(j));
                }
                for (int j = i + 1; j < expression.size(); ++j) {
                    exp2.push_back(expression.at(j));
                }

                resolveExpression(exp1);
                resolveExpression(exp2);

                outputOperator(expression.at(operatorPos)->getLexeme());

                break;
            }
        }
    }
}

// Takes a vector of arguments and determines their types. Returns a vector of types.
void Parser::extractTypesOfArguments(const vector<vector<Token*>> &arguments, vector<string> &types) {

    int sizeArgs = arguments.size();
    int size;

    if (arguments.empty()) {
        types.emplace_back("void");
        return;
    }
    else if (arguments.size() == 1 && arguments.at(0).empty()) {
        types.emplace_back("void");
        return;
    }

    string argumentType = "-1";

    types.clear();

    bool insideArgs = false;

    bool insideSqArgs = false;

    for (int i = 0; i < sizeArgs; ++i) {

        size = arguments.at(i).size();

        for (int j = 0; j < size; ++j) {

            int countBrackets = 0;

            int countSqBrackets = 0;

            int argBracket = -1;

            int argSqBracket = -1;

            if (arguments.at(i).at(j)->getLexeme() == "(") {
                ++countBrackets;
            }
            else if (arguments.at(i).at(j)->getLexeme() == ")") {
                --countBrackets;
            }
            else if (arguments.at(i).at(j)->getLexeme() == "[") {
                ++countBrackets;
            }
            else if (arguments.at(i).at(j)->getLexeme() == "]") {
                --countBrackets;
            }

            // Arguments are present when there is : identifier(... or identifier[
            if (arguments.at(i).at(j)->getLexeme() == "(" && (j - 1) >= 0 &&
                arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                argBracket = countBrackets;
                insideArgs = true;
            }
            else if (arguments.at(i).at(j)->getLexeme() == "[" && (j - 1) >= 0 &&
                     arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                argSqBracket = countSqBrackets;
                insideSqArgs = true;
            }
            else if (arguments.at(i).at(j)->getLexeme() == ")") {
                if (countBrackets == (argBracket - 1)) {
                    insideArgs = false;
                }
            }
            else if (arguments.at(i).at(j)->getLexeme() == "]") {
                if (countSqBrackets == (argSqBracket - 1)) {
                    insideSqArgs = false;
                }
            }

            // A variable which returns boolean can be used here.
            if (arguments.at(i).at(j)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                Symbol *currentSymbol = findSymbolNoError(arguments.at(i).at(j)->getLexeme());
                // Return format is: ArrayId[]
                if (currentSymbol->getType() == "Array" && ((j + 1) < size) &&
                    arguments.at(i).at(j + 1)->getLexeme() == "[") {
                    bool insideArgs2 = false;
                    bool insideSqArgs2 = false;
                    bool agree = true;

                    int countBrackets2 = 0;
                    int countSqBrackets2 = 0;

                    int argBracket2 = -1;
                    int argSqBracket2 = -1;

                    for (int k = 0; k < size; ++k) {

                        if (arguments.at(i).at(k)->getLexeme() == "(") {
                            ++countBrackets2;
                        }
                        else if (arguments.at(i).at(k)->getLexeme() == ")") {
                            --countBrackets2;
                        }
                        if (arguments.at(i).at(k)->getLexeme() == "[") {
                            ++countSqBrackets2;
                        }
                        else if (arguments.at(i).at(k)->getLexeme() == "]") {
                            --countSqBrackets2;
                        }

                        // Arguments are present when there is : identifier(...
                        if (arguments.at(i).at(k)->getLexeme() == "(" && (j - 1) >= 0 &&
                            arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                            argBracket2 = countBrackets2;
                            insideArgs2 = true;
                        }
                        else if (arguments.at(i).at(j)->getLexeme() == ")") {
                            if (countBrackets2 == (argBracket2 - 1)) {
                                insideArgs2 = false;
                            }
                        }
                        // Arguments are present when there is : identifier[...
                        if (arguments.at(i).at(k)->getLexeme() == "[" && (j - 1) >= 0 &&
                            arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                            argSqBracket2 = countSqBrackets2;
                            insideSqArgs2 = true;
                        }
                        else if (arguments.at(i).at(j)->getLexeme() == "]") {
                            if (countSqBrackets2 == (argSqBracket2 - 1)) {
                                insideSqArgs2 = false;
                            }
                        }

                        // If any of these operators are present alongside with an array[], then the return type is not Array. (Or does not default to the requiredType).
                        if ((arguments.at(i).at(j)->getLexeme() == "&" || arguments.at(i).at(j)->getLexeme() == "|" ||
                             arguments.at(i).at(j)->getLexeme() == "=" ||
                             arguments.at(i).at(j)->getLexeme() == ">" || arguments.at(i).at(j)->getLexeme() == "<" ||
                             arguments.at(i).at(j)->getLexeme() == "~" ||
                             arguments.at(i).at(j)->getLexeme() == "true" || arguments.at(i).at(j)->getLexeme() == "false" ||
                             arguments.at(i).at(j)->getLexeme() == "+" || arguments.at(i).at(j)->getLexeme() == "-" ||
                             arguments.at(i).at(j)->getLexeme() == "*" ||
                             arguments.at(i).at(j)->getLexeme() == "/") && !insideArgs2 && !insideSqArgs2) {

                            agree = false;
                        }
                    }

                    // Return type and required type are most likely the same or it is only possible to determine during run-time.
                    if (agree) {
                        argumentType = "__allow";
                    }
                }
                    // If there is an array without square brackets, or simply an array return, then the return type is Array.
                else if (currentSymbol->getType() == "Array" &&
                         ((((j + 1) < size) && arguments.at(i).at(j + 1)->getLexeme() != "[") || ((j + 1) >= size))) {
                    argumentType = "Array";
                    break;
                }

                if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                     currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") &&
                    currentSymbol->getType() == "boolean") {

                    argumentType = "boolean";
                    break;
                }
            }

            if ((arguments.at(i).at(j)->getLexeme() == "&" || arguments.at(i).at(j)->getLexeme() == "|" ||
                 arguments.at(i).at(j)->getLexeme() == "=" ||
                 arguments.at(i).at(j)->getLexeme() == ">" || arguments.at(i).at(j)->getLexeme() == "<" ||
                 arguments.at(i).at(j)->getLexeme() == "~" ||
                 arguments.at(i).at(j)->getLexeme() == "true" || arguments.at(i).at(j)->getLexeme() == "false") &&
                !insideArgs && !insideSqArgs) {

                argumentType = "boolean";
                break;
            }
        }

        insideArgs = false;
        insideSqArgs = false;

        // Still undetermined.
        if (argumentType == "-1") {
            for (int j = 0; j < size; ++j) {

                int countBrackets = 0;

                int countSqBrackets = 0;

                int argBracket = -1;

                int argSqBracket = -1;

                if (arguments.at(i).at(j)->getLexeme() == "(") {
                    ++countBrackets;
                }
                else if (arguments.at(i).at(j)->getLexeme() == ")") {
                    --countBrackets;
                }
                else if (arguments.at(i).at(j)->getLexeme() == "[") {
                    ++countBrackets;
                }
                else if (arguments.at(i).at(j)->getLexeme() == "]") {
                    --countBrackets;
                }

                // Arguments are present when there is : identifier(... or identifier[
                if (arguments.at(i).at(j)->getLexeme() == "(" && (j - 1) >= 0 &&
                    arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                    argBracket = countBrackets;
                    insideArgs = true;
                }
                else if (arguments.at(i).at(j)->getLexeme() == "[" && (j - 1) >= 0 &&
                         arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                    argSqBracket = countSqBrackets;
                    insideSqArgs = true;
                }
                else if (arguments.at(i).at(j)->getLexeme() == ")") {
                    if (countBrackets == (argBracket - 1)) {
                        insideArgs = false;
                    }
                }
                else if (arguments.at(i).at(j)->getLexeme() == "]") {
                    if (countSqBrackets == (argSqBracket - 1)) {
                        insideSqArgs = false;
                    }
                }

                // A variable which returns boolean can be used here.
                if (arguments.at(i).at(j)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                    Symbol *currentSymbol = findSymbolNoError(arguments.at(i).at(j)->getLexeme());

                    if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                         currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") &&
                        currentSymbol->getType() == "int") {

                        argumentType = "int";
                        break;
                    }
                }

                if ((arguments.at(i).at(j)->getLexeme() == "+" || arguments.at(i).at(j)->getLexeme() == "-" ||
                     arguments.at(i).at(j)->getLexeme() == "*" ||
                     arguments.at(i).at(j)->getLexeme() == "/" ||
                     arguments.at(i).at(j)->getType() == Token::TokenTypes::number) && !insideArgs && !insideSqArgs) {

                    argumentType = "int";
                    break;
                }
            }
        }

        insideArgs = false;
        insideSqArgs = false;

        // And still undetermined.
        if (argumentType == "-1") {
            for (int j = 0; j < size; ++j) {

                int countBrackets = 0;

                int countSqBrackets = 0;

                int argBracket = -1;

                int argSqBracket = -1;

                if (arguments.at(i).at(j)->getLexeme() == "(") {
                    ++countBrackets;
                }
                else if (arguments.at(i).at(j)->getLexeme() == ")") {
                    --countBrackets;
                }
                else if (arguments.at(i).at(j)->getLexeme() == "[") {
                    ++countBrackets;
                }
                else if (arguments.at(i).at(j)->getLexeme() == "]") {
                    --countBrackets;
                }

                // Arguments are present when there is : identifier(... or identifier[
                if (arguments.at(i).at(j)->getLexeme() == "(" && (j - 1) >= 0 &&
                    arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                    argBracket = countBrackets;
                    insideArgs = true;
                }
                else if (arguments.at(i).at(j)->getLexeme() == "[" && (j - 1) >= 0 &&
                         arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                    argSqBracket = countSqBrackets;
                    insideSqArgs = true;
                }
                else if (arguments.at(i).at(j)->getLexeme() == ")") {
                    if (countBrackets == (argBracket - 1)) {
                        insideArgs = false;
                    }
                }
                else if (arguments.at(i).at(j)->getLexeme() == "]") {
                    if (countSqBrackets == (argSqBracket - 1)) {
                        insideSqArgs = false;
                    }
                }

                // A variable which returns boolean can be used here.
                if (arguments.at(i).at(j)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                    Symbol *currentSymbol = findSymbolNoError(arguments.at(i).at(j)->getLexeme());

                    if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                         currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") &&
                        currentSymbol->getType() == "char") {

                        argumentType = "char";
                        break;
                    }
                }

                if (arguments.at(i).at(j)->getType() == Token::TokenTypes::string_literal && !insideArgs && !insideSqArgs) {
                    if (arguments.at(i).at(j)->getLexeme().size() == 1 || arguments.at(i).at(j)->getLexeme().empty()) {
                        argumentType = "char";
                    } else {
                        argumentType = "String";
                    }
                    break;
                }
            }
        }

        // Could be a user defined type.
        if (argumentType == "-1") {
            for (int j = 0; j < size; ++j) {
                if (arguments.at(i).at(j)->getLexeme() == "." || arguments.at(i).at(j)->getType() == Token::TokenTypes::id) {

                    if (arguments.at(i).at(j)->getLexeme() == "." && ((j - 1) >= 0) && ((j + 1) < size)) {

                        Symbol *classSymbol;
                        Symbol *funOrVarSymbol;

                        if (arguments.at(i).at(j - 1)->getType() == Token::TokenTypes::id) {
                            classSymbol = findSymbolNoError(arguments.at(i).at(j - 1)->getLexeme());

                            if (classSymbol->getKind() == "class") {
                                // Return format: thisClass.function() or thisClass.var
                                if (classSymbol->getName() == className) {
                                    funOrVarSymbol = findSymbolNoError((arguments.at(i).at(j + 1)->getLexeme()));
                                    if (funOrVarSymbol->getType() != "not_found") {
                                        argumentType = funOrVarSymbol->getType();
                                        break;
                                    }
                                }
                                    // Return format: otherClass.function() or otherClass.var
                                else {
                                    if (classSymbol->getKind() != "not_found") {
                                        int classPos = findClassPosition(arguments.at(i).at(j - 1)->getLexeme());

                                        Symbol *thisSymbol = findSymbolInClassNoError(arguments.at(i).at(j + 1)->getLexeme(),
                                                                                      classPos);

                                        if (thisSymbol->getType() != "not_found") {
                                            argumentType = thisSymbol->getType();
                                            break;
                                        }
                                    }
                                }
                            }
                                // Return format: varClass.function() or varClass.var
                            else if (classSymbol->getKind() == "var" || classSymbol->getKind() == "static" ||
                                     classSymbol->getKind() == "field") {

                                string varClassName = classSymbol->getType();
                                Symbol *varClassSymbol = findSymbolNoError(varClassName);

                                if (varClassName == className) {
                                    funOrVarSymbol = findSymbolNoError((arguments.at(i).at(j + 1)->getLexeme()));
                                    if (funOrVarSymbol->getType() != "not_found") {
                                        argumentType = funOrVarSymbol->getType();
                                        break;
                                    }
                                }
                                    // Return format: otherClassVar.function() or otherClassVar.var
                                else {
                                    if (varClassSymbol->getKind() != "not_found") {
                                        int classPos = findClassPosition(varClassName);

                                        Symbol *thisSymbol = findSymbolInClassNoError(arguments.at(i).at(j + 1)->getLexeme(),
                                                                                      classPos);

                                        if (thisSymbol->getType() != "not_found") {
                                            argumentType = thisSymbol->getType();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } else if (arguments.at(i).at(j)->getType() == Token::TokenTypes::id) {
                        Symbol *idSymbol = findSymbolNoError(arguments.at(i).at(j)->getLexeme());

                        // Class type.
                        if (idSymbol->getType() != "int" && idSymbol->getType() != "char" &&
                            idSymbol->getType() != "boolean") {

                            if (idSymbol->getType() != "not_found") {
                                argumentType = idSymbol->getType();
                                break;
                            }
                        }
                    }
                }

            }
        }

        types.push_back(argumentType);
        argumentType = "-1";
    }
}

// Type -> int | char | boolean | identifier
void Parser::type() {
    Token *nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "int" && nextToken->getLexeme() != "char" &&
        nextToken->getLexeme() != "boolean") {

        if(nextToken->getType() != Token::TokenTypes::id) {
            cout << "Line: " << line << "\n";
            cout << "Error: expected a type identifier or an identifier token at this position.\n";
            exit(EXIT_FAILURE);
        }

        string idName = nextToken->getLexeme();

        Symbol* idSymbol = findSymbolNoError(idName);

        // Class is not found.
        if (idSymbol->getKind() == "not_found") {
            throw runtime_error("Line: " + to_string(line) + ", type: " + idName + " was not found");
        }

        if (idSymbol->getKind() != "class") {
            cout << "Line: " << line << "\n";
            cout << "Error: the type, " << idName << ", must be a class, or another primitive data type allowed by Jack.\n";
            exit(EXIT_FAILURE);
        }

    }
}

// ClassVarDeclar -> (static | field) Type identifier {, identifier} ;
void Parser::classVarDeclar() {
    Token *nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "static" && nextToken->getLexeme() != "field") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected static or field token at this position\n";
        exit(EXIT_FAILURE);
    }

    // Save the kind of the class variable.
    string kindV = nextToken->getLexeme();

    // Save the type of the class variable.
    string typeV = lexer->PeekNextToken()->getLexeme();

    // Type identifier has to be present next.
    type();

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    // Followed by identifier.
    if (nextToken->getType() != Token::TokenTypes::id) {
        cout << "Line: " << line << "\n";
        cout << "Error: expected identifier token at this position\n";
        exit(EXIT_FAILURE);
    }

    string name = nextToken->getLexeme();

    // Check if something was declared with the same name already.
    searchForRedeclaration(name);

    addToTable(name, typeV, kindV);

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Possible to have multiple identifiers separated by commas.
    while (nextToken->getLexeme() == ",") {
        // Consume the comma.
        nextToken = lexer->GetNextToken();

        // Check next token -> must be an identifier.
        nextToken = lexer->GetNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            ++line;
            
            nextToken = lexer->GetNextToken();
        }
        // End of file is reached.
        if (nextToken->getType() == Token::TokenTypes::eof) {
            cout << "Line: " << line << "\n";
            cout << "Error: unexpected end of file.\n";
            exit(EXIT_FAILURE);
        }

        if (nextToken->getType() != Token::TokenTypes::id) {
            cout << "Line: " << line << "\n";
            cout << "Error: expected identifier token at this position\n";
            exit(EXIT_FAILURE);
        }

        name = nextToken->getLexeme();

        // Check if something was declared with the same name already.
        searchForRedeclaration(name);

        addToTable(name, typeV, kindV);

        // Peek next token to check for another comma.
            nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ";") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ';' before '" << nextToken->getLexeme() << "'\n";
        exit(EXIT_FAILURE);
    }
}

// ParamList -> Type identifier {, Type identifier} | ε
void Parser::paramList() {
    Token *nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    string name;
    string typeV;
    string kindV = "argument";
    bool initialised = true;

    // Parameter list could be empty, so if there is a parameter, then proceed checking, else carry on parsing.
    if (nextToken->getLexeme() == "int" || nextToken->getLexeme() == "char" ||
        nextToken->getLexeme() == "boolean" || nextToken->getType() == Token::TokenTypes::id) {

        nextToken = lexer->PeekNextToken();
        typeV = nextToken->getLexeme();

        // Parameter list has to start with a type identifier.
        type();

        currentFunctionSymbol->addArgumentType(typeV);

        // Followed by identifier.
        nextToken = lexer->GetNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            ++line;
            
            nextToken = lexer->GetNextToken();
        }
        // End of file is reached.
        if (nextToken->getType() == Token::TokenTypes::eof) {
            cout << "Line: " << line << "\n";
            cout << "Error: unexpected end of file.\n";
            exit(EXIT_FAILURE);
        }

        if (nextToken->getType() != Token::TokenTypes::id) {
            cout << "Line: " << line << "\n";
            cout << "Error: expected identifier token at this position\n";
            exit(EXIT_FAILURE);
        }

        name = nextToken->getLexeme();

        nextToken = lexer->PeekNextToken();

        searchForRedeclaration(name);

        addToTable(name, typeV, kindV, initialised);

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }

        // Possible to have multiple parameters separated by commas.
        while (nextToken->getLexeme() == ",") {
            // Consume the comma.
            nextToken = lexer->GetNextToken();

            nextToken = lexer->PeekNextToken();

            // Consuming new lines and increasing the line counter for correct error display.
            while (nextToken->getType() == Token::TokenTypes::new_line) {
                // Consume new_line.
                nextToken = lexer->GetNextToken();

                ++line;

                // Check if new_line appears again.
                nextToken = lexer->PeekNextToken();
            }

            typeV = nextToken->getLexeme();

            // Check next token -> must be a type.
            type();

            currentFunctionSymbol->addArgumentType(typeV);

            // Type is followed by identifier.
            nextToken = lexer->GetNextToken();

            // Consuming new lines and increasing the line counter for correct error display.
            while (nextToken->getType() == Token::TokenTypes::new_line) {
                ++line;
                
                nextToken = lexer->GetNextToken();
            }
            // End of file is reached.
            if (nextToken->getType() == Token::TokenTypes::eof) {
                cout << "Line: " << line << "\n";
                cout << "Error: unexpected end of file.\n";
                exit(EXIT_FAILURE);
            }

            if (nextToken->getType() != Token::TokenTypes::id) {
                cout << "Line: " << line << "\n";
                cout << "Error: expected identifier token at this position\n";
                exit(EXIT_FAILURE);
            }

            name = nextToken->getLexeme();

            searchForRedeclaration(name);

            addToTable(name, typeV, kindV, initialised);

            // Peek next token to check for another comma.
            nextToken = lexer->PeekNextToken();

            // Consuming new lines and increasing the line counter for correct error display.
            while (nextToken->getType() == Token::TokenTypes::new_line) {
                // Consume new_line.
                nextToken = lexer->GetNextToken();

                ++line;

                // Check if new_line appears again.
                nextToken = lexer->PeekNextToken();
            }
        }
    }
}

// VarDeclarStatement -> var Type identifier { , identifier } ;
void  Parser::varDeclarStatement() {
    Token *nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    // Statement should start with var.
    if (nextToken->getLexeme() != "var") {
        cout << "Error: expected var token at this position\n";
        exit(EXIT_FAILURE);
    }

    nextToken = lexer->PeekNextToken();

    string name;
    string typeV = nextToken->getLexeme();
    string kindV = "var";

    // Followed by type.
    type();

    // Type is followed by identifier.
    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getType() != Token::TokenTypes::id) {
        cout << "Line: " << line << "\n";
        cout << "Error: expected identifier token at this position\n";
        exit(EXIT_FAILURE);
    }

    name = nextToken->getLexeme();

    searchForRedeclaration(name);

    addToTable(name, typeV, kindV);

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Possible to have multiple identifiers separated by commas.
    while (nextToken->getLexeme() == ",") {
        // Consume the comma.
        nextToken = lexer->GetNextToken();

        // Check next token -> must be an identifier.
        nextToken = lexer->GetNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            ++line;
            
            nextToken = lexer->GetNextToken();
        }
        // End of file is reached.
        if (nextToken->getType() == Token::TokenTypes::eof) {
            cout << "Line: " << line << "\n";
            cout << "Error: unexpected end of file.\n";
            exit(EXIT_FAILURE);
        }

        if (nextToken->getType() != Token::TokenTypes::id) {
            cout << "Line: " << line << "\n";
            cout << "Error: expected identifier token at this position\n";
            exit(EXIT_FAILURE);
        }

        name = nextToken->getLexeme();

        searchForRedeclaration(name);

        addToTable(name, typeV, kindV);

        // Peek next token to check for another comma.
        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ";") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ';' before '" << nextToken->getLexeme() << "'\n";
        exit(EXIT_FAILURE);
    }
}

// Expression -> RelationalExpression { ( & | | ) RelationalExpression }
void Parser::expression() {
    Token *nextToken;

    // Expressions start with a relational expression.
    relationalExpression();

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Possible to have multiple relational expressions separated by & or |.
    while (nextToken->getLexeme() == "&" || nextToken->getLexeme() == "|") {
        // Consume the & or | symbol.
        nextToken = lexer->GetNextToken();

        // Now there has to be another relational expression.
        relationalExpression();

        // Peek next token to check for another & or |.
        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }

}

// RelationalExpression -> ArithmeticExpression { ( = | > | < ) ArithmeticExpression }
void Parser::relationalExpression() {
    Token *nextToken;

    // Relational expressions start with an arithmetic expression.
    arithmeticExpression();

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Possible to have multiple arithmetic expressions separated by =, > or <.
    while (nextToken->getLexeme() == "=" || nextToken->getLexeme() == ">" || nextToken->getLexeme() == "<") {
        // Consume the =, > or < symbol.
        nextToken = lexer->GetNextToken();

        // Now there has to be another arithmetic expression.
        arithmeticExpression();

        // Peek next token to check for another =, > or <.
        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;
            

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }
}

// ArithmeticExpression -> Term { ( + | - ) Term }
void Parser::arithmeticExpression() {
    Token *nextToken;

    // Arithmetic expressions start with a term.
    term();

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Possible to have multiple terms separated by + or -.
    while (nextToken->getLexeme() == "+" || nextToken->getLexeme() == "-") {
        // Consume the + or - symbol.
        nextToken = lexer->GetNextToken();

        // Now there has to be another term.
        term();

        // Peek next token to check for another + or -.
        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }
}

// Term -> Factor { ( * | / ) Factor }
void Parser::term() {
    Token *nextToken;

    // Terms start with a factor.
    factor();

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Possible to have multiple factors separated by * or /.
    while (nextToken->getLexeme() == "*" || nextToken->getLexeme() == "/") {
        // Consume the * or / symbol.
        nextToken = lexer->GetNextToken();

        // Now there has to be another term.
        factor();

        // Peek next token to check for another * or /.
        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }
}

// Factor -> ( - | ~ | ε ) Operand
void Parser::factor() {
    Token *nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Factors start with an optional - or ~ symbol.
    if (nextToken->getLexeme() == "-" || nextToken->getLexeme() == "~") {
        // Consume - or ~ symbol.
        nextToken = lexer->GetNextToken();
    }

    // Followed by an operand.
    operand();
}

// Operand -> integerConstant | identifier [.identifier ] [ [ Expression ] | (ExpressionList ) ] | (Expression) | stringLiteral | true | false | null | this
void Parser::operand() {
    Token *nextToken = lexer->GetNextToken();

    bool hasArrayAccess = false;
    bool takeFirst = true;

    // Vector of array expression tokens.
    vector<Token*> arExpression;

    Symbol *identifierSymbol = nullptr;

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    // (expression) type operand.
    if (nextToken->getLexeme() == "(") {
        expression();

        nextToken = lexer->GetNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            ++line;

            nextToken = lexer->GetNextToken();
        }
        // End of file is reached.
        if (nextToken->getType() == Token::TokenTypes::eof) {
            cout << "Line: " << line << "\n";
            cout << "Error: unexpected end of file.\n";
            exit(EXIT_FAILURE);
        }

        if (nextToken->getLexeme() != ")") {
            cout << "Line: " << line << "\n";
            cout << "Error: expected ) at this position\n";
            exit(EXIT_FAILURE);
        }
    }

    // identifier[.identifier][ [expression] | (expressionList) ]   type operand.
    else if (nextToken->getType() == Token::TokenTypes::id ||
            (nextToken->getType() == Token::TokenTypes::keyword && nextToken->getLexeme() == "this")) {

        string idName = nextToken->getLexeme();

        // Could be a class symbol, a variable or a function.
        Symbol* idSymbol = findSymbolNoError(idName);

        Token *secondId = nullptr;
        Symbol *secondSymbol = nullptr;

        Symbol *calledFunction = nullptr;

        string calledFunctionName = "-1";

        vector<vector<Token*>> requiredArguments;
        vector<string> requiredTypes;
        vector<vector<Token*>> givenArguments;
        vector<string> givenTypes;

        bool functionDeclaredLater = false;

        string thisClassName = idName;

        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }

        if (nextToken->getLexeme() == ".") {
            // Consume .
            nextToken = lexer->GetNextToken();

            takeFirst = false;

            if (idSymbol->getKind() != "class") {
                // If not, then this variable has a class type, which is allowed.
                if (idSymbol->getType() == "int" || idSymbol->getType() == "boolean" || idSymbol->getType() == "char") {
                    cout << "Line: " << line << "\n";
                    cout << "Error: " << idName << " is not a class.\n";
                    exit(EXIT_FAILURE);
                }
                thisClassName = idSymbol->getType();
            }

            nextToken = lexer->GetNextToken();

            // Consuming new lines and increasing the line counter for correct error display.
            while (nextToken->getType() == Token::TokenTypes::new_line) {
                ++line;

                nextToken = lexer->GetNextToken();
            }
            // End of file is reached.
            if (nextToken->getType() == Token::TokenTypes::eof) {
                cout << "Line: " << line << "\n";
                cout << "Error: unexpected end of file.\n";
                exit(EXIT_FAILURE);
            }

            // Next token must be an identifier.
            if (nextToken->getType() != Token::TokenTypes::id) {
                cout << "Line: " << line << "\n";
                cout << "Error: expected an identifier at this position\n";
                exit(EXIT_FAILURE);
            }

            secondId = nextToken;

            nextToken = lexer->PeekNextToken();

            int classPosition = findClassPosition(thisClassName);

            // class.var
            if (nextToken->getLexeme() != "(") {
                Symbol *varSymbol = findSymbolInClass(secondId->getLexeme(), classPosition);

                secondSymbol = varSymbol;

                if (!varSymbol->getInitialised()) {
                    cout << "Line: " << line << "\n";
                    cout << "Warning: variable " << varSymbol->getName() << " might have been used before initialised.\n";
                }
            }
            // class.function()
            else {
                Symbol *funSymbol = findFunctionInClass(secondId->getLexeme(), classPosition);

                calledFunction = funSymbol;
                calledFunctionName = secondId->getLexeme();

                if (funSymbol->getKind() == "not_found") {
                    if (thisClassName == className) {
                        if (!checkIfFunctionDeclared(secondId->getLexeme())) {
                            cout << "Line: " << line << "\n";
                            cout << "Error: class (" << idName << ") has not got function ("
                                     << secondId->getLexeme() << ") declared.\n";
                            exit(EXIT_FAILURE);

                        }
                        // Function is declared later.
                        else {
                            functionDeclaredLater = true;

                            extractArgumentsUndeclaredFunction(calledFunctionName, requiredTypes);

                            if (requiredTypes.empty()) {
                                requiredTypes.emplace_back("void");
                            }
                        }

                    }
                    else {
                        cout << "Line: " << line << "\n";
                        cout << "Error: class (" << idName << ") has not got function ("
                             << secondId->getLexeme() << ") declared.\n";
                        exit(EXIT_FAILURE);
                    }
                }
            }
        }
        else {

            nextToken = lexer->PeekNextToken();

            // var
            if (nextToken->getLexeme() != "(") {
                // Variable not found.
                if (idSymbol->getKind() == "not_found") {
                    cout << "Line: " << line << "\n";
                    cout << "Error: Symbol with name " << idName << " does not exist";
                    exit(EXIT_FAILURE);
                }
                else {
                    if (!idSymbol->getInitialised()) {
                        cout << "Line: " << line << "\n";
                        cout << "Warning: variable " << idSymbol->getName()
                             << " might have been used before initialised.\n";
                    }
                }
            }
            // function()
            else {

                Symbol *funSymbol = findSymbolNoError(idName);

                calledFunction = funSymbol;
                calledFunctionName = idName;

                if (funSymbol->getKind() == "not_found") {
                    if (!checkIfFunctionDeclared(idName)) {
                        cout << "Line: " << line << "\n";
                        cout << "Error: class (" << className << ") has not got function ("
                                 << idName << ") declared.\n";
                        exit(EXIT_FAILURE);
                    }
                    // Function is declared later.
                    else {
                        functionDeclaredLater = true;
                        extractArgumentsUndeclaredFunction(idName, requiredTypes);
                        calledFunctionName = idName;
                        if (requiredTypes.empty()) {
                            requiredTypes.emplace_back("void");
                        }
                    }
                }
            }
        }

        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }

        Symbol *identifier;

        if (takeFirst) {
            identifier = idSymbol;
        }
        else {
            identifier = secondSymbol;
        }

        if (nextToken->getLexeme() == "[") {
            // Consume [ .
            nextToken = lexer->GetNextToken();

            hasArrayAccess = true;

            // The identifier must be an array.
            if (identifier->getType() != "Array") {
                cout << "Line: " << line << "\n";
                cout << "Error: " << identifier->getName() << " is not an Array";
                exit(EXIT_FAILURE);
            }

            identifierSymbol = identifier;

            // Save the expression inside square brackets until the closing bracket or end of file. (Checks grammar later).

            int tokenPos = 0;

            while (lexer->PeekNextTokenPlus(tokenPos)->getLexeme() != "]" &&
                   lexer->PeekNextTokenPlus(tokenPos)->getType() != Token::TokenTypes::eof) {

                arExpression.push_back(lexer->PeekNextTokenPlus(tokenPos));

                ++tokenPos;
            }

            expression();

            // Checks that the expression inside brackets evaluates to an integer.

            bool insideArgs = false;
            bool insideSqArgs = false;

            bool intExpression = false;

            int sizeExpression = arExpression.size();

            for (int i = 0; i < sizeExpression; ++i) {

                int countBrackets = 0;

                int countSqBrackets = 0;

                int argBracket = -1;
                int argSqBracket = -1;

                if (arExpression.at(i)->getLexeme() == "(") {
                    ++countBrackets;
                }
                else if (arExpression.at(i)->getLexeme() == ")") {
                    --countBrackets;
                }
                else if (arExpression.at(i)->getLexeme() == "[") {
                    ++countSqBrackets;
                }
                else if (arExpression.at(i)->getLexeme() == "]") {
                    --countSqBrackets;
                }

                // Arguments are present when there is : identifier(...
                if (arExpression.at(i)->getLexeme() == "(" && (i-1) >= 0 && arExpression.at(i-1)->getType() == Token::TokenTypes::id) {
                    argBracket = countBrackets;
                    insideArgs = true;
                }
                // Arguments are present when there is : identifier[...
                else if (arExpression.at(i)->getLexeme() == "[" && (i-1) >= 0 && arExpression.at(i-1)->getType() == Token::TokenTypes::id) {
                    argSqBracket = countSqBrackets;
                    insideSqArgs = true;
                }
                else if (arExpression.at(i)->getLexeme() == ")") {
                    if (countBrackets == (argBracket - 1)) {
                        insideArgs = false;
                    }
                }
                else if (arExpression.at(i)->getLexeme() == "]") {
                    if (countSqBrackets == (argSqBracket - 1)) {
                        insideSqArgs = false;
                    }
                }

                // A variable which returns boolean can be used here.
                if (arExpression.at(i)->getType() == Token::TokenTypes::id && !insideArgs) {
                    Symbol* currentSymbol = findSymbolNoError(arExpression.at(i)->getLexeme());

                    if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                         currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") && currentSymbol->getType() == "int") {

                        intExpression = true;
                        break;
                    }
                }

                if ((arExpression.at(i)->getLexeme() == "+" || arExpression.at(i)->getLexeme() == "-" || arExpression.at(i)->getLexeme() == "*" ||
                     arExpression.at(i)->getLexeme() == "/" || arExpression.at(i)->getType() == Token::TokenTypes::number) && !insideArgs) {

                    intExpression = true;
                    break;
                }

                // There is an array inside this array expression: can't check what's inside during compile time, so let it be.
                if (arExpression.at(i)->getLexeme() == "[" && !insideArgs) {
                    intExpression = true;
                    break;
                }
            }

            if (!intExpression) {
                for (int i = 0; i < sizeExpression; ++i) {
                    if (arExpression.at(i)->getLexeme() == ".") {
                        if ((i-1) >= 0) {
                            Token *classToken = arExpression.at(i-1);

                            int classPosition = findClassPosition(classToken->getLexeme());

                            if ((i+1) < sizeExpression) {
                                Token *functionToken = arExpression.at(i+1);
                                Symbol *functionSymbol = findFunctionInClass(functionToken->getLexeme(), classPosition);

                                if (functionSymbol->getKind() == "not_found") {
                                    cout << "Line: " << line << "\n";
                                    cout << "Error: class " << classToken->getLexeme() << " does not have a definition for function "<< functionToken->getLexeme() <<".\n";
                                    exit(EXIT_FAILURE);
                                }
                                else {
                                    if (functionSymbol->getType() != "int") {
                                        cout << "Line: " << line << "\n";
                                        cout << "Error: in class " << classToken->getLexeme() << ", function "<< functionToken->getLexeme()
                                        <<" returns " << functionSymbol->getType() << "\n, however it should return int to resolve as an index for the array.\n";
                                        exit(EXIT_FAILURE);
                                    }
                                }
                            }
                        }
                        intExpression = true;
                    }
                }
            }

            // Array index does not return an integer.
            if (!intExpression) {
                cout << "Line: " << line << "\n";
                cout << "Error: the index of the array " << idName << " does not represent an integer value.\n";
                exit(EXIT_FAILURE);
            }

            nextToken = lexer->GetNextToken();

            // Consuming new lines and increasing the line counter for correct error display.
            while (nextToken->getType() == Token::TokenTypes::new_line) {
                ++line;

                nextToken = lexer->GetNextToken();
            }
            // End of file is reached.
            if (nextToken->getType() == Token::TokenTypes::eof) {
                cout << "Line: " << line << "\n";
                cout << "Error: unexpected end of file.\n";
                exit(EXIT_FAILURE);
            }

            if (nextToken->getLexeme() != "]") {
                cout << "Line: " << line << "\n";
                cout << "Error: expected ] at this position\n";
                exit(EXIT_FAILURE);
            }
        }
        // (expressionList)
        else if (nextToken->getLexeme() == "(") {
            // Consume ( .
            nextToken = lexer->GetNextToken();

            // If declared later, requiredTypes is already done above.
            if (!functionDeclaredLater && calledFunction != nullptr) {
                requiredTypes = calledFunction->getArguments();

                // Means a void return.
                if (requiredTypes.empty()) {
                    requiredTypes.emplace_back("void");
                }
            }


            Token *listToken = lexer->PeekNextToken();
            givenArguments.emplace_back();
            int tokenNumber = 0;
            int argNumber = 0;

            int numBrack = 1;

            // Save the expressionList
            while (numBrack != 0 && listToken->getType() != Token::TokenTypes::eof) {
                if (listToken->getLexeme() == ",") {
                    givenArguments.emplace_back();
                    ++argNumber;
                }
                else if (listToken->getLexeme() == "(") {
                    ++numBrack;
                    givenArguments.at(argNumber).push_back(listToken);
                }
                else if (listToken->getLexeme() == ")") {
                    --numBrack;
                    if (numBrack != 0) {
                        givenArguments.at(argNumber).push_back(listToken);
                    }
                }
                else {
                    givenArguments.at(argNumber).push_back(listToken);
                }
                ++tokenNumber;
                listToken = lexer->PeekNextTokenPlus(tokenNumber);
            }

            expressionList();

            for (int i = 0; i < givenArguments.size(); ++i) {
                resolveExpression(givenArguments.at(i));
            }

            extractTypesOfArguments(givenArguments, givenTypes);

            if (givenTypes.empty()) {
                givenTypes.emplace_back("void");
            }
            if (requiredTypes.empty()) {
                requiredTypes.emplace_back("void");
            }

            for (int i = 0; i < requiredTypes.size(); ++i) {
                if (requiredTypes.at(i) == "void" && requiredTypes.size() > 1) {
                    requiredTypes.erase(requiredTypes.begin() + i);
                }
            }

            if (givenTypes.size() != requiredTypes.size()) {
                cout << "Line: " << line << "\n";
                cout << "Error: function: " << calledFunctionName << " expects " << requiredTypes.size() << " arguments, " <<
                     "however, " << givenTypes.size() << " were given.\n";
                exit(EXIT_FAILURE);
            }
            else {
                for (int i = 0; i < givenTypes.size(); ++i) {

                    // Char can be used as an integer.
                    // Integer can be used as a char.
                    // Null can be assigned to anything.
                    // Boolean can be assigned to int and int can be assigned to boolean.
                    // Boolean can be assigned to char and char can be assigned to boolean.

                    if (givenTypes.at(i) != requiredTypes.at(i) && givenTypes.at(i) != "__allow" && requiredTypes.at(i) != "Array" &&
                        !(givenTypes.at(i) == "boolean" && requiredTypes.at(i) == "int") && !(givenTypes.at(i) == "int" && requiredTypes.at(i) == "boolean") &&
                        !(givenTypes.at(i) == "char" && requiredTypes.at(i) == "int") && !(givenTypes.at(i) == "int" && requiredTypes.at(i) == "char") &&
                        !(givenTypes.at(i) == "char" && requiredTypes.at(i) == "boolean") && !(givenTypes.at(i) == "boolean" && requiredTypes.at(i) == "char")) {

                        cout << "Line: " << line << "\n";
                        cout << "Argument " << i << " (Type: " << givenTypes.at(i) <<") does not have a required type (" << requiredTypes.at(i) << ") in function " << calledFunctionName << endl;
                        exit(EXIT_FAILURE);
                    }
                }
            }

            nextToken = lexer->GetNextToken();

            // Consuming new lines and increasing the line counter for correct error display.
            while (nextToken->getType() == Token::TokenTypes::new_line) {
                ++line;

                nextToken = lexer->GetNextToken();
            }
            // End of file is reached.
            if (nextToken->getType() == Token::TokenTypes::eof) {
                cout << "Line: " << line << "\n";
                cout << "Error: unexpected end of file.\n";
                exit(EXIT_FAILURE);
            }

            if (nextToken->getLexeme() != ")") {
                cout << "Line: " << line << "\n";
                cout << "Error: expected ) at this position\n";
                exit(EXIT_FAILURE);
            }
        }

    }
    else {
        if (nextToken->getType() != Token::TokenTypes::number && nextToken->getType() != Token::TokenTypes::string_literal &&
            nextToken->getLexeme() != "true" && nextToken->getLexeme() != "false" && nextToken->getLexeme() != "null" &&
            nextToken->getLexeme() != "this") {

            cout << "Line: " << line << "\n";
            cout << "Error: invalid operand\n";
            exit(EXIT_FAILURE);
        }
    }

    if (hasArrayAccess) {

        resolveExpression(arExpression);

        if (takeFirst) {
            if (identifierSymbol->getKind() == "var") {
                vmf << "push local " << identifierSymbol->getPosition() << endl;
            }
            else if (identifierSymbol->getKind() == "field") {
                vmf << "push this " << identifierSymbol->getPosition() << endl;
            }
            // Argument variable.
            else if (identifierSymbol->getKind() == "argument") {
                // If the current function is method, follow the symbol table positions for arguments.
                if (determineFunctionKind(determineFunctionPosition()) == "method") {
                    vmf << "push " << identifierSymbol->getKind() << " " << identifierSymbol->getPosition() << endl;
                }
                // Else decrease the position of arguments by 1. (Because 'this' takes up the first position).
                else {
                    vmf << "push " << identifierSymbol->getKind() << " " << identifierSymbol->getPosition() - 1 << endl;
                }
            }
            // Static variable.
            else  {
                vmf << "push " << identifierSymbol->getKind() << " " << identifierSymbol->getPosition() << endl;
            }

            vmf << "add" << endl;

            // Popping the result of the array location to 'that';
            vmf << "pop pointer 1" << endl;
        }
    }
}

// ExpressionList -> Expression { , Expression } | ε
void Parser::expressionList() {
    Token *nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Check if expression list starts with a factor, because the list could be empty.
    if (nextToken->getLexeme() == "-" || nextToken->getLexeme() == "~" || nextToken->getLexeme() == "true" ||
        nextToken->getLexeme() == "false" || nextToken->getLexeme() == "null" || nextToken->getLexeme() == "this" ||
        nextToken->getType() == Token::TokenTypes::number || nextToken->getType() == Token::TokenTypes::string_literal ||
        nextToken->getType() == Token::TokenTypes::id || nextToken->getLexeme() == "(") {

        expression();

        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }

        // It is possible to have multiple expressions separated by , .
        while (nextToken->getLexeme() == ",") {
            // Consume , .
            nextToken = lexer->GetNextToken();

            // Expected another expression now.
            expression();

            nextToken = lexer->PeekNextToken();

            // Consuming new lines and increasing the line counter for correct error display.
            while (nextToken->getType() == Token::TokenTypes::new_line) {
                // Consume new_line.
                nextToken = lexer->GetNextToken();

                ++line;

                // Check if new_line appears again.
                nextToken = lexer->PeekNextToken();
            }
        }
    }
}

// ReturnStatemnt -> return [ expression ] ;
void Parser::returnStatement() {
    Token *nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }
    
    // Return statement must start with return.
    if (nextToken->getLexeme() != "return") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected return keyword at this position\n";
        exit(EXIT_FAILURE);
    }

    int position = determineFunctionPosition();

    string requiredType = determineFunctionType(position);

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Save the expression inside the return statement until the semicolon or end of file. (Checks grammar later).

    int tokenPos = 0;

    // Vector of subroutine body tokens.
    vector<Token*> retVector;

    while (lexer->PeekNextTokenPlus(tokenPos)->getType() != Token::TokenTypes::eof &&
           lexer->PeekNextTokenPlus(tokenPos)->getLexeme() != ";") {

        retVector.push_back(lexer->PeekNextTokenPlus(tokenPos));

        ++tokenPos;
    }

    // Check if expression list starts with a factor, because the return statement could contain eg. return; .
    if (nextToken->getLexeme() == "-" || nextToken->getLexeme() == "~" || nextToken->getLexeme() == "true" ||
        nextToken->getLexeme() == "false" || nextToken->getLexeme() == "null" || nextToken->getLexeme() == "this" ||
        nextToken->getType() == Token::TokenTypes::number || nextToken->getType() == Token::TokenTypes::string_literal ||
        nextToken->getType() == Token::TokenTypes::id || nextToken->getLexeme() == "(") {

        expression();
    }

    // Making sure that the return type matches the required return type of the function.

    string returnType = "-1";

    bool check = true;

    if (retVector.size() == 1) {
        if (retVector.at(0)->getLexeme() == "this") {
            Symbol *thisSymbol = findSymbol("this");

            returnType = thisSymbol->getType();
            check = false;
        }
    }

    if (check) {
        // Return nothing.
        if (retVector.empty()) {
            returnType = "void";
        } else {
            int size = retVector.size();

            bool insideArgs = false;
            bool insideSqArgs = false;

            for (int i = 0; i < size; ++i) {

                int countBrackets = 0;

                int countSqBrackets = 0;

                int argBracket = -1;
                int argSqBracket = -1;

                if (retVector.at(i)->getLexeme() == "(") {
                    ++countBrackets;
                }
                else if (retVector.at(i)->getLexeme() == ")") {
                    --countBrackets;
                }
                else if (retVector.at(i)->getLexeme() == "[") {
                    ++countSqBrackets;
                }
                else if (retVector.at(i)->getLexeme() == "]") {
                    --countSqBrackets;
                }

                // Arguments are present when there is : identifier(...
                if (retVector.at(i)->getLexeme() == "(" && (i-1) >= 0 && retVector.at(i-1)->getType() == Token::TokenTypes::id) {
                    argBracket = countBrackets;
                    insideArgs = true;
                }
                    // Arguments are present when there is : identifier[...
                else if (retVector.at(i)->getLexeme() == "[" && (i-1) >= 0 && retVector.at(i-1)->getType() == Token::TokenTypes::id) {
                    argSqBracket = countSqBrackets;
                    insideSqArgs = true;
                }
                else if (retVector.at(i)->getLexeme() == ")") {
                    if (countBrackets == (argBracket - 1)) {
                        insideArgs = false;
                    }
                }
                else if (retVector.at(i)->getLexeme() == "]") {
                    if (countSqBrackets == (argSqBracket - 1)) {
                        insideSqArgs = false;
                    }
                }

                // A variable which returns boolean can be used here.
                if (retVector.at(i)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                    Symbol *currentSymbol = findSymbolNoError(retVector.at(i)->getLexeme());

                    // Return format is: ArrayId[]
                    if (currentSymbol->getType() == "Array" && ((i + 1) < size) &&
                        retVector.at(i + 1)->getLexeme() == "[") {
                        bool insideArgs2 = false;
                        bool insideSqArgs2 = false;
                        bool agree = true;

                        int countBrackets2 = 0;
                        int countSqBrackets2 = 0;

                        int argBracket2 = -1;
                        int argSqBracket2 = -1;

                        for (int k = 0; k < size; ++k) {

                            if (retVector.at(k)->getLexeme() == "(") {
                                ++countBrackets2;
                            }
                            else if (retVector.at(k)->getLexeme() == ")") {
                                --countBrackets2;
                            }
                            if (retVector.at(k)->getLexeme() == "[") {
                                ++countSqBrackets2;
                            }
                            else if (retVector.at(k)->getLexeme() == "]") {
                                --countSqBrackets2;
                            }

                            // Arguments are present when there is : identifier(...
                            if (retVector.at(k)->getLexeme() == "(" && (k - 1) >= 0 &&
                                retVector.at(k - 1)->getType() == Token::TokenTypes::id) {
                                argBracket2 = countBrackets2;
                                insideArgs2 = true;
                            }
                            else if (retVector.at(k)->getLexeme() == ")") {
                                if (countBrackets2 == (argBracket2 - 1)) {
                                    insideArgs2 = false;
                                }
                            }
                            // Arguments are present when there is : identifier[...
                            if (retVector.at(k)->getLexeme() == "[" && (k - 1) >= 0 &&
                                retVector.at(k - 1)->getType() == Token::TokenTypes::id) {
                                argSqBracket2 = countSqBrackets2;
                                insideSqArgs2 = true;
                            }
                            else if (retVector.at(k)->getLexeme() == "]") {
                                if (countSqBrackets2 == (argSqBracket2 - 1)) {
                                    insideSqArgs2 = false;
                                }
                            }

                            // If any of these operators are present alongside with an array[], then the return type is not Array. (Or does not default to the requiredType).
                            if ((retVector.at(i)->getLexeme() == "&" || retVector.at(i)->getLexeme() == "|" ||
                                 retVector.at(i)->getLexeme() == "=" ||
                                 retVector.at(i)->getLexeme() == ">" || retVector.at(i)->getLexeme() == "<" ||
                                 retVector.at(i)->getLexeme() == "~" ||
                                 retVector.at(i)->getLexeme() == "true" || retVector.at(i)->getLexeme() == "false" ||
                                 retVector.at(i)->getLexeme() == "+" || retVector.at(i)->getLexeme() == "-" ||
                                 retVector.at(i)->getLexeme() == "*" ||
                                 retVector.at(i)->getLexeme() == "/") && !insideArgs2 && !insideSqArgs2) {

                                agree = false;
                            }
                        }

                        // Return type and required type are most likely the same or it is only possible to determine during run-time.
                        if (agree) {
                            returnType = requiredType;
                        }
                    }
                        // If there is an array without square brackets, or simply an array return, then the return type is Array.
                    else if (currentSymbol->getType() == "Array" &&
                             ((((i + 1) < size) && retVector.at(i + 1)->getLexeme() != "[") || ((i + 1) >= size))) {
                        returnType = "Array";
                        break;
                    }

                    if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                         currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") &&
                        currentSymbol->getType() == "boolean") {

                        returnType = "boolean";
                        break;
                    }
                }

                if ((retVector.at(i)->getLexeme() == "&" || retVector.at(i)->getLexeme() == "|" ||
                     retVector.at(i)->getLexeme() == "=" ||
                     retVector.at(i)->getLexeme() == ">" || retVector.at(i)->getLexeme() == "<" ||
                     retVector.at(i)->getLexeme() == "~" ||
                     retVector.at(i)->getLexeme() == "true" || retVector.at(i)->getLexeme() == "false") &&
                    !insideArgs && !insideSqArgs) {

                    returnType = "boolean";
                    break;
                }
            }

            insideArgs = false;

            insideSqArgs = false;

            // Still undetermined.
            if (returnType == "-1") {
                for (int i = 0; i < size; ++i) {

                    int countBrackets = 0;

                    int countSqBrackets = 0;

                    int argBracket = -1;
                    int argSqBracket = -1;

                    if (retVector.at(i)->getLexeme() == "(") {
                        ++countBrackets;
                    }
                    else if (retVector.at(i)->getLexeme() == ")") {
                        --countBrackets;
                    }
                    else if (retVector.at(i)->getLexeme() == "[") {
                        ++countSqBrackets;
                    }
                    else if (retVector.at(i)->getLexeme() == "]") {
                        --countSqBrackets;
                    }

                    // Arguments are present when there is : identifier(...
                    if (retVector.at(i)->getLexeme() == "(" && (i-1) >= 0 && retVector.at(i-1)->getType() == Token::TokenTypes::id) {
                        argBracket = countBrackets;
                        insideArgs = true;
                    }
                        // Arguments are present when there is : identifier[...
                    else if (retVector.at(i)->getLexeme() == "[" && (i-1) >= 0 && retVector.at(i-1)->getType() == Token::TokenTypes::id) {
                        argSqBracket = countSqBrackets;
                        insideSqArgs = true;
                    }
                    else if (retVector.at(i)->getLexeme() == ")") {
                        if (countBrackets == (argBracket - 1)) {
                            insideArgs = false;
                        }
                    }
                    else if (retVector.at(i)->getLexeme() == "]") {
                        if (countSqBrackets == (argSqBracket - 1)) {
                            insideSqArgs = false;
                        }
                    }

                    // A variable which returns boolean can be used here.
                    if (retVector.at(i)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                        Symbol *currentSymbol = findSymbolNoError(retVector.at(i)->getLexeme());

                        if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                             currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") &&
                            currentSymbol->getType() == "int") {

                            returnType = "int";
                            break;
                        }
                    }

                    if ((retVector.at(i)->getLexeme() == "+" || retVector.at(i)->getLexeme() == "-" ||
                         retVector.at(i)->getLexeme() == "*" ||
                         retVector.at(i)->getLexeme() == "/" ||
                         retVector.at(i)->getType() == Token::TokenTypes::number) && !insideArgs && !insideSqArgs) {

                        returnType = "int";
                        break;
                    }
                }
            }

            insideArgs = false;
            insideSqArgs = false;

            // And still undetermined.
            if (returnType == "-1") {
                for (int i = 0; i < size; ++i) {

                    int countBrackets = 0;

                    int countSqBrackets = 0;

                    int argBracket = -1;
                    int argSqBracket = -1;

                    if (retVector.at(i)->getLexeme() == "(") {
                        ++countBrackets;
                    }
                    else if (retVector.at(i)->getLexeme() == ")") {
                        --countBrackets;
                    }
                    else if (retVector.at(i)->getLexeme() == "[") {
                        ++countSqBrackets;
                    }
                    else if (retVector.at(i)->getLexeme() == "]") {
                        --countSqBrackets;
                    }

                    // Arguments are present when there is : identifier(...
                    if (retVector.at(i)->getLexeme() == "(" && (i-1) >= 0 && retVector.at(i-1)->getType() == Token::TokenTypes::id) {
                        argBracket = countBrackets;
                        insideArgs = true;
                    }
                        // Arguments are present when there is : identifier[...
                    else if (retVector.at(i)->getLexeme() == "[" && (i-1) >= 0 && retVector.at(i-1)->getType() == Token::TokenTypes::id) {
                        argSqBracket = countSqBrackets;
                        insideSqArgs = true;
                    }
                    else if (retVector.at(i)->getLexeme() == ")") {
                        if (countBrackets == (argBracket - 1)) {
                            insideArgs = false;
                        }
                    }
                    else if (retVector.at(i)->getLexeme() == "]") {
                        if (countSqBrackets == (argSqBracket - 1)) {
                            insideSqArgs = false;
                        }
                    }

                    // A variable which returns boolean can be used here.
                    if (retVector.at(i)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                        Symbol *currentSymbol = findSymbolNoError(retVector.at(i)->getLexeme());

                        if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                             currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") &&
                            currentSymbol->getType() == "char") {

                            returnType = "char";
                            break;
                        }
                    }

                    if (retVector.at(i)->getType() == Token::TokenTypes::string_literal && !insideArgs && !insideSqArgs) {
                        if (retVector.at(i)->getLexeme().size() == 1 || retVector.at(i)->getLexeme().empty()) {
                            returnType = "char";
                        } else {
                            returnType = "String";
                        }
                        break;
                    }
                }
            }

            // Could be a user defined type.
            if (returnType == "-1") {
                for (int i = 0; i < size; ++i) {
                    if (retVector.at(i)->getLexeme() == "." || retVector.at(i)->getType() == Token::TokenTypes::id) {

                        if (retVector.at(i)->getLexeme() == "." && ((i-1) >= 0) && ((i+1) < size)) {

                            Symbol *classSymbol;
                            Symbol *funOrVarSymbol;

                            if (retVector.at(i-1)->getType() == Token::TokenTypes::id ||
                               (retVector.at(i-1)->getType() == Token::TokenTypes::keyword && retVector.at(i-1)->getLexeme() == "this")) {
                                classSymbol = findSymbolNoError(retVector.at(i - 1)->getLexeme());

                                if (classSymbol->getKind() == "class") {
                                    // Return format: thisClass.function() or thisClass.var
                                    if (classSymbol->getName() == className) {
                                        funOrVarSymbol = findSymbolNoError((retVector.at(i + 1)->getLexeme()));
                                        if (funOrVarSymbol->getType() != "not_found") {
                                            returnType = funOrVarSymbol->getType();
                                            break;
                                        }
                                    }
                                    // Return format: otherClass.function() or otherClass.var
                                    else {
                                        if (classSymbol->getKind() != "not_found") {
                                            int classPos = findClassPosition(retVector.at(i-1)->getLexeme());

                                            Symbol *thisSymbol = findSymbolInClassNoError(retVector.at(i+1)->getLexeme(), classPos);

                                            if (thisSymbol->getType() != "not_found") {
                                                returnType = thisSymbol->getType();
                                                break;
                                            }
                                        }
                                    }
                                }
                                // Return format: varClass.function() or varClass.var
                                else if (classSymbol->getKind() == "var" || classSymbol->getKind() == "static" ||
                                           classSymbol->getKind() == "field") {

                                    string varClassName = classSymbol->getType();
                                    Symbol *varClassSymbol = findSymbolNoError(varClassName);

                                    if (varClassName == className) {
                                        funOrVarSymbol = findSymbolNoError((retVector.at(i + 1)->getLexeme()));
                                        if (funOrVarSymbol->getType() != "not_found") {
                                            returnType = funOrVarSymbol->getType();
                                            break;
                                        }
                                    }
                                    // Return format: otherClassVar.function() or otherClassVar.var
                                    else {
                                        if (varClassSymbol->getKind() != "not_found") {
                                            int classPos = findClassPosition(varClassName);

                                            Symbol *thisSymbol = findSymbolInClassNoError(retVector.at(i+1)->getLexeme(), classPos);

                                            if (thisSymbol->getType() != "not_found") {
                                                returnType = thisSymbol->getType();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (retVector.at(i)->getType() == Token::TokenTypes::id) {
                            Symbol *idSymbol = findSymbolNoError(retVector.at(i)->getLexeme());

                            // Class type.
                            if (idSymbol->getType() != "int" && idSymbol->getType() != "char" &&
                                idSymbol->getType() != "boolean") {

                                if (idSymbol->getType() != "not_found") {
                                    returnType = idSymbol->getType();
                                    break;
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    if (returnType != requiredType) {
        cout << "Line: " << line << "\n";
        cout << "Error: return type of the function/method ("<< determineFunctionName(position) <<") (" << requiredType <<") does not match the returned type (" << returnType <<").\n";
        exit(EXIT_FAILURE);
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ";") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ';' before '" << nextToken->getLexeme() << "'\n";
        exit(EXIT_FAILURE);
    }

    // If return statement was in the scope of the whole function, then the function returns in all paths. (id = 2)
    if (currentlyInFunctionScope()) {
        returnInAllPaths = 2;
    }

    resolveExpression(retVector);

    if (!inSubroutineCall) {
        if (requiredType == "void" || retVector.empty()) {
            vmf << "push constant 0" << endl;
            vmf << "return" << endl;
        } else {
            vmf << "return" << endl;
        }
    }

    hasReturn = true;
}

// SubroutineCall -> identifier [ . identifier ] ( ExpressionList )
void Parser::subroutineCall() {
    Token *nextToken = lexer->GetNextToken();

    inSubroutineCall = true;

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    // Subroutine call should start with an identifier.
    if (nextToken->getType() != Token::TokenTypes::id) {
        cout << "Line: " << line << "\n";
        cout << "Error: expected an identifier at this position\n";
        exit(EXIT_FAILURE);
    }

    // The call must be in the form function() or method() or class.function() or class.method().
    string idName = nextToken->getLexeme();

    Symbol *idSymbol = findSymbolNoError(idName);

    Symbol *calledFunctionSymbol = nullptr;
    Symbol *calledClassSymbol = nullptr;

    string calledFunctionName;

    vector<string> requiredTypes;
    vector<vector<Token*>> givenArguments;
    vector<string> givenTypes;

    bool idClass = false;
    bool idFunction = false;
    bool varClass = false;
    bool pureFunction = true;

    bool undeclaredYet = false;

    string varClassName = "-1";

    // Do statement has the format: do function();
    if (lexer->PeekNextToken()->getLexeme() == "(") {
        idFunction = true;
    }

    // Class is not found.
    if (idSymbol->getKind() == "not_found") {
        if (!idFunction) {
            throw runtime_error("Line: " + to_string(line) + ", type: " + idName + " was not found");
        }
        // Function in this class could be undeclared yet.
        else {
            if (!checkIfFunctionDeclared(idName)) {
                throw runtime_error("Line: " + to_string(line) + ", function: " + idName + " was not found");
            }
            // Declared later.
            else {
                // Append requiredTypes
                extractArgumentsUndeclaredFunction(idName, requiredTypes);

                calledFunctionName = idName;

                undeclaredYet = true;
            }
        }
    }
    else if (idSymbol->getKind() == "class") {
        idClass = true;
    }
    else if (idSymbol->getKind() == "function" || idSymbol->getKind() == "method") {
        idFunction = true;
        calledFunctionSymbol = idSymbol;
        calledFunctionName = idName;
    }
    else if (idSymbol->getKind() == "var" || idSymbol->getKind() == "static" || idSymbol->getKind() == "field" || idSymbol->getKind() == "argument") {
        // A variable's type could be a class.
        Symbol *checkSymbol = findSymbolNoError(idSymbol->getType());

        if (checkSymbol->getKind() != "class") {
            cout << "Line: " << line << "\n";
            cout << "Error: a subroutine call must be of the form function() or method() or class.function() or class.method().\n";
            exit(EXIT_FAILURE);
        }
        varClassName = checkSymbol->getName();
        varClass = true;
    }
    else {
        cout << "Line: " << line << "\n";
        cout << "Error: a subroutine call must be of the form function() or method() or class.function() or class.method().\n";
        exit(EXIT_FAILURE);
    }

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    if (nextToken->getLexeme() == ".") {
        // Consume .
        nextToken = lexer->GetNextToken();

        calledClassSymbol = idSymbol;

        // x. -> is not possible, where x is not a class.
        if (!idClass && !varClass) {
            cout << "Line: " << line << "\n";
            cout << "Error: a subroutine call must be of the form function() or method() or class.function() or class.method().\n";
            exit(EXIT_FAILURE);
        }

        pureFunction = false;

        nextToken = lexer->GetNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            ++line;
            
            nextToken = lexer->GetNextToken();
        }
        // End of file is reached.
        if (nextToken->getType() == Token::TokenTypes::eof) {
            cout << "Line: " << line << "\n";
            cout << "Error: unexpected end of file.\n";
            exit(EXIT_FAILURE);
        }

        // Next token must be an identifier.
        if (nextToken->getType() != Token::TokenTypes::id) {
            cout << "Line: " << line << "\n";
            cout << "Error: expected an identifier at this position\n";
            exit(EXIT_FAILURE);
        }

        string nextIdName = nextToken->getLexeme();

        int classNumber;

        if (varClass) {
            classNumber = findClassPosition(varClassName);
        }
        else {
            classNumber = findClassPosition(idName);
        }

        if (classNumber == -1) {
            throw runtime_error("Line: " + to_string(line) + ", type: " + idName + " was not found");
        }

        Symbol *functionSymbol = findFunctionInClass(nextIdName, classNumber);

        calledFunctionSymbol = functionSymbol;

        calledFunctionName = nextIdName;

        // If  call is of the form <<This Class>>.function(), then check further if there is a declaration of the function.
        if (idName == className && functionSymbol->getKind() == "not_found") { // Not found or not found just yet!
            if (!checkIfFunctionDeclared(nextIdName)) {
                cout << "Line: " << line << "\n";
                cout << "Error: class (" << idName << ") does not have a declaration for function (" << nextIdName << ")\n";
                exit(EXIT_FAILURE);
            }
            else {
                extractArgumentsUndeclaredFunction(nextIdName, requiredTypes);
                undeclaredYet = true;
            }
        }
        // The function is not declared in this class.
        else if (functionSymbol->getKind() == "not_found") {
            cout << "Line: " << line << "\n";
            cout << "Error: class (" << idName << ") does not have a declaration for function (" << nextIdName << ")\n";
            exit(EXIT_FAILURE);
        }

    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "(") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected a ( at this position\n";
        exit(EXIT_FAILURE);
    }

    if (!undeclaredYet && calledFunctionSymbol != nullptr) {
        requiredTypes = calledFunctionSymbol->getArguments();
    }

    Token *listToken = lexer->PeekNextToken();
    givenArguments.emplace_back();
    int tokenNumber = 0;
    int argNumber = 0;

    int numBrack = 1;

    while (numBrack != 0 && listToken->getType() != Token::TokenTypes::eof) {
        if (listToken->getLexeme() == ",") {
            givenArguments.emplace_back();
            ++argNumber;
        }
        else if (listToken->getLexeme() == "(") {
            ++numBrack;
            givenArguments.at(argNumber).push_back(listToken);
        }
        else if (listToken->getLexeme() == ")") {
            --numBrack;
            if (numBrack != 0) {
                givenArguments.at(argNumber).push_back(listToken);
            }
        }
        else {
            givenArguments.at(argNumber).push_back(listToken);
        }
        ++tokenNumber;
        listToken = lexer->PeekNextTokenPlus(tokenNumber);
    }

    // Call a function which will determine the types of arguments passed. Store it in givenTypes.

    if (pureFunction) {
        vmf << "push pointer 0" << endl;
    }
    else {
        if (varClass) {
            if (calledClassSymbol->getKind() == "var") {
                vmf << "push local " << calledClassSymbol->getPosition() << endl;
            }
            else if (calledClassSymbol->getKind() == "field") {
                vmf << "push this " << calledClassSymbol->getPosition() << endl;
            }
            // Argument variable.
            else if (calledClassSymbol->getKind() == "argument") {
                // If the current function is method, follow the symbol table positions for arguments.
                if (determineFunctionKind(determineFunctionPosition()) == "method") {
                    vmf << "push " << calledClassSymbol->getKind() << " " << calledClassSymbol->getPosition() << endl;
                }
                // Else decrease the position of arguments by 1. (Because 'this' takes up the first position).
                else {
                    vmf << "push " << calledClassSymbol->getKind() << " " << calledClassSymbol->getPosition() - 1 << endl;
                }
            }
            // Static variable.
            else  {
                vmf << "push " << calledClassSymbol->getKind() << " " << calledClassSymbol->getPosition() << endl;
            }
        }
    }

    expressionList();

    for (int i = 0; i < givenArguments.size(); ++i) {
        resolveExpression(givenArguments.at(i));
    }

    extractTypesOfArguments(givenArguments, givenTypes);

    // Means a void return.
    if (givenTypes.empty()) {
        givenTypes.emplace_back("void");
    }
    if (requiredTypes.empty()) {
        requiredTypes.emplace_back("void");
    }

    for (int i = 0; i < requiredTypes.size(); ++i) {
        if (requiredTypes.at(i) == "void" && requiredTypes.size() > 1) {
            requiredTypes.erase(requiredTypes.begin() + i);
        }
    }

    if (givenTypes.size() != requiredTypes.size()) {
        cout << "Line: " << line << "\n";
        cout << "Error: function: " << calledFunctionName << " expects " << requiredTypes.size() << " arguments, " <<
                "however, " << givenTypes.size() << " were given.\n";
        exit(EXIT_FAILURE);
    }
    else {
        for (int i = 0; i < givenTypes.size(); ++i) {
            // Char can be used as an integer.
            // Integer can be used as a char.
            // Null can be assigned to anything.
            // Boolean can be assigned to int and int can be assigned to boolean.
            // Boolean can be assigned to char and char can be assigned to boolean.

            if (givenTypes.at(i) != requiredTypes.at(i) && givenTypes.at(i) != "__allow" && requiredTypes.at(i) != "Array" &&
                !(givenTypes.at(i) == "boolean" && requiredTypes.at(i) == "int") && !(givenTypes.at(i) == "int" && requiredTypes.at(i) == "boolean") &&
                !(givenTypes.at(i) == "char" && requiredTypes.at(i) == "int") && !(givenTypes.at(i) == "int" && requiredTypes.at(i) == "char") &&
                !(givenTypes.at(i) == "char" && requiredTypes.at(i) == "boolean") && !(givenTypes.at(i) == "boolean" && requiredTypes.at(i) == "char")) {

                cout << "Line: " << line << "\n";
                cout << "Argument " << i << " (Type: " << givenTypes.at(i) <<") does not have a required type (" << requiredTypes.at(i) << ") in function " << calledFunctionName << endl;
                exit(EXIT_FAILURE);
            }
        }
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ")") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected a ) at this position\n";
        exit(EXIT_FAILURE);
    }

    if (pureFunction) {

        if (undeclaredYet) {
            // Determine the number of arguments of yet undeclared function.

            vector<string> typeArguments;

            extractArgumentsUndeclaredFunction(calledFunctionName, typeArguments);

            vmf << "call " << className << "." << calledFunctionName << " " << typeArguments.size() + 1 << endl;
        }
        // Function in this class has already been declared.
        else {
            vmf << "call " << className << "." << calledFunctionName << " " << to_string(calledFunctionSymbol->getArguments().size() + 1) << endl;
        }
    }
    else {
        if (idClass) {
            // Calls without passing the object to the function.
            vmf << "call " << calledClassSymbol->getName() << "." << calledFunctionSymbol->getName() << " " << to_string(calledFunctionSymbol->getArguments().size()) << endl;
        }
        else if (varClass) {
            vmf << "call " << calledClassSymbol->getType() << "." << calledFunctionSymbol->getName() << " " << to_string(calledFunctionSymbol->getArguments().size() + 1) << endl;
        }
    }

    vmf << "pop temp 0" << endl;

    inSubroutineCall = false;
}

// DoStatement -> do SubroutineCall ;
void Parser::doStatement() {
    Token *nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    // Do statements start with do.
    if (nextToken->getLexeme() != "do") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected do at this position\n";
        exit(EXIT_FAILURE);
    }

    subroutineCall();

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ";") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ';' before '" << nextToken->getLexeme() << "'\n";
        exit(EXIT_FAILURE);
    }
}

// WhileStatement -> while ( Expression ) { {Statement} }
void Parser::whileStatement() {
    Token *nextToken = lexer->GetNextToken();

    inMeaningfulIf = 0;

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    // While statements start with while.
    if (nextToken->getLexeme() != "while") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected while at this position\n";
        exit(EXIT_FAILURE);
    }

    ++countWhiles;

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "(") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ( at this position\n";
        exit(EXIT_FAILURE);
    }

    vmf << "label WHILE_COND" << countWhiles << endl;
    numberWhiles.push_back(countWhiles);

    int pos = -1;
    int bracketsCount = 0;
    Token *tempToken = nextToken;

    vector<Token*> exp;

    while (true) {

        if (tempToken->getLexeme() == "(") {
            ++bracketsCount;
            exp.push_back(tempToken);
        }
        else if (tempToken->getLexeme() == ")") {
            --bracketsCount;
            if (bracketsCount == 0) {
                // Finished copying.
                break;
            }
            exp.push_back(tempToken);
        }
        else {
            exp.push_back(tempToken);
        }
        ++pos;
        tempToken = lexer->PeekNextTokenPlus(pos);
    }

    expression();

    resolveExpression(exp);

    vmf << "not" << endl;
    vmf << "if-goto WHILE_END" << countWhiles << endl;


    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ")") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ) at this position\n";
        exit(EXIT_FAILURE);
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "{") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected { at this position\n";
        exit(EXIT_FAILURE);
    }

    currentProgramState.at(thisClassPosition).emplace_back();
    ++currentTable;

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // 0 or more statements are expected now.
    while (nextToken->getLexeme() == "var" || nextToken->getLexeme() == "let" || nextToken->getLexeme() == "if" ||
        nextToken->getLexeme() == "while" || nextToken->getLexeme() == "do" || nextToken->getLexeme() == "return") {

        statement();

        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "}") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected } at this position\n";
        exit(EXIT_FAILURE);
    }

    vmf << "goto WHILE_COND" << numberWhiles.back() << endl;
    vmf << "label WHILE_END" << numberWhiles.back() << endl;

    numberWhiles.pop_back();

    --currentTable;
}

// IfStatement -> if ( Expression ) { {Statement} } [else { {Statement} }]
void Parser::ifStatement() {
    Token *nextToken = lexer->GetNextToken();

    bool returnInIf = false;
    bool returnInElse = false;
    bool hasElse = false;

    if (currentlyInFunctionScope()) {
        inMeaningfulIf = 1;
    }

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    // If statements start with if.
    if (nextToken->getLexeme() != "if") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected if at this position\n";
        exit(EXIT_FAILURE);
    }

    ++countIfs;

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "(") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ( at this position\n";
        exit(EXIT_FAILURE);
    }

    int pos = -1;
    int bracketsCount = 0;
    Token *tempToken = nextToken;

    vector<Token*> exp;

    while (true) {

        if (tempToken->getLexeme() == "(") {
            ++bracketsCount;
            exp.push_back(tempToken);
        }
        else if (tempToken->getLexeme() == ")") {
            --bracketsCount;
            if (bracketsCount == 0) {
                // Finished copying.
                break;
            }
            exp.push_back(tempToken);
        }
        else {
            exp.push_back(tempToken);
        }
        ++pos;
        tempToken = lexer->PeekNextTokenPlus(pos);
    }

    expression();

    resolveExpression(exp);

    numberIfs.push_back(countIfs);

    vmf << "if-goto IF_TRUE" << numberIfs.back() << endl;
    vmf << "goto IF_FALSE" << numberIfs.back() << endl;
    vmf << "label IF_TRUE" << numberIfs.back() << endl;

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ")") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ) at this position\n";
        exit(EXIT_FAILURE);
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "{") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected { at this position\n";
        exit(EXIT_FAILURE);
    }

    currentProgramState.at(thisClassPosition).emplace_back();
    ++currentTable;

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // 0 or more statements are expected now.
    while (nextToken->getLexeme() == "var" || nextToken->getLexeme() == "let" || nextToken->getLexeme() == "if" ||
           nextToken->getLexeme() == "while" || nextToken->getLexeme() == "do" || nextToken->getLexeme() == "return") {

        // There is a return statement in if statement, one path is satisfied.
        if (nextToken->getLexeme() == "return" || nextToken->getLexeme() == "if") {
            returnInIf = true;
        }

        statement();

        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "}") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected } at this position\n";
        exit(EXIT_FAILURE);
    }

    --currentTable;

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    if (nextToken->getLexeme() == "else") {
        // Consume else.
        nextToken = lexer->GetNextToken();

        // Followed by { {statement} }
        nextToken = lexer->GetNextToken();

        vmf << "goto IF_END" << numberIfs.back() << endl;
        vmf << "label IF_FALSE" << numberIfs.back() << endl;

        hasElse = true;

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            ++line;
            
            nextToken = lexer->GetNextToken();
        }
        // End of file is reached.
        if (nextToken->getType() == Token::TokenTypes::eof) {
            cout << "Line: " << line << "\n";
            cout << "Error: unexpected end of file.\n";
            exit(EXIT_FAILURE);
        }

        if (nextToken->getLexeme() != "{") {
            cout << "Line: " << line << "\n";
            cout << "Error: expected { at this position\n";
            exit(EXIT_FAILURE);
        }

        currentProgramState.at(thisClassPosition).emplace_back();
        ++currentTable;

        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }

        // 0 or more statements are expected now.
        while (nextToken->getLexeme() == "var" || nextToken->getLexeme() == "let" || nextToken->getLexeme() == "if" ||
               nextToken->getLexeme() == "while" || nextToken->getLexeme() == "do" || nextToken->getLexeme() == "return") {

            // There is a return statement in the corresponding else statement, another path is satisfied.
            if (nextToken->getLexeme() == "return" || nextToken->getLexeme() == "if") {
                returnInElse = true;
            }

            statement();

            nextToken = lexer->PeekNextToken();

            // Consuming new lines and increasing the line counter for correct error display.
            while (nextToken->getType() == Token::TokenTypes::new_line) {
                // Consume new_line.
                nextToken = lexer->GetNextToken();

                ++line;

                // Check if new_line appears again.
                nextToken = lexer->PeekNextToken();
            }
        }

        nextToken = lexer->GetNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            ++line;
            
            nextToken = lexer->GetNextToken();
        }
        // End of file is reached.
        if (nextToken->getType() == Token::TokenTypes::eof) {
            cout << "Line: " << line << "\n";
            cout << "Error: unexpected end of file.\n";
            exit(EXIT_FAILURE);
        }

        if (nextToken->getLexeme() != "}") {
            cout << "Line: " << line << "\n";
            cout << "Error: expected } at this position\n";
            exit(EXIT_FAILURE);
        }

        --currentTable;
    }

    if (!hasElse) {
        vmf << "label IF_FALSE" << numberIfs.back() << endl;
        numberIfs.pop_back();
    }
    else {
        vmf << "label IF_END" << numberIfs.back() << endl;
        numberIfs.pop_back();
    }

    // It is a meaningful if for return statements (Scope in which the if statement in the function scope).
    if (inMeaningfulIf == 1) {
        if (returnInAllPaths != 0) {
            if (returnInIf && returnInElse) {
                if (returnInAllPaths != 2) {
                    returnInAllPaths = 1;
                }
            }
            else {
                if (returnInAllPaths != 2) {
                    returnInAllPaths = 0;
                }
            }
        }
    }
}

// LetStatemnt -> let identifier [ [ Expression ] ] = Expression ;
void Parser::letStatement() {
    Token *nextToken = lexer->GetNextToken();

    // Type is left as -1 if the LHS is an array, so types are not checked.
    string typeLHS = "-1";

    // -1 is the value indicating that the rhs type hasn't been changed.
    string typeRHS = "-1";

    bool hasArrayAccess = false;

    // Vector of array expression tokens.
    vector<Token*> arExpression;

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    // Let statements start with let.
    if (nextToken->getLexeme() != "let") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected let at this position\n";
        exit(EXIT_FAILURE);
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getType() != Token::TokenTypes::id) {
        cout << "Line: " << line << "\n";
        cout << "Error: expected an identifier at this position\n";
        exit(EXIT_FAILURE);
    }

    string idName = nextToken->getLexeme();

    Symbol* idSymbol = findSymbol(idName);

    Symbol *identifierSymbol = idSymbol;

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    if (nextToken->getLexeme() == "[") {
        // Consume [ .
        nextToken = lexer->GetNextToken();

        hasArrayAccess = true;

        // If the identifier is not an array, then it cannot be accessed with square brackets.
        if (idSymbol->getType() != "Array") {
            cout << "Line: " << line << "\n";
            cout << "Error: " << idSymbol->getName() << " is not an Array\n";
            exit(EXIT_FAILURE);
        }

        // Save the expression inside square brackets until the closing bracket or end of file. (Checks grammar later).

        int tokenPos = 0;

        while (lexer->PeekNextTokenPlus(tokenPos)->getLexeme() != "]" &&
               lexer->PeekNextTokenPlus(tokenPos)->getType() != Token::TokenTypes::eof) {

            arExpression.push_back(lexer->PeekNextTokenPlus(tokenPos));

            ++tokenPos;
        }

        expression();

        // Checks that the expression inside brackets evaluates to an integer.

        bool insideArgs = false;
        bool insideSqArgs = false;

        bool intExpression = false;

        int sizeExpression = arExpression.size();

        for (int i = 0; i < sizeExpression; ++i) {

            int countBrackets = 0;

            int countSqBrackets = 0;

            int argBracket = -1;
            int argSqBracket = -1;

            if (arExpression.at(i)->getLexeme() == "(") {
                ++countBrackets;
            }
            else if (arExpression.at(i)->getLexeme() == ")") {
                --countBrackets;
            }
            else if (arExpression.at(i)->getLexeme() == "[") {
                ++countSqBrackets;
            }
            else if (arExpression.at(i)->getLexeme() == "]") {
                --countSqBrackets;
            }

            // Arguments are present when there is : identifier(...
            if (arExpression.at(i)->getLexeme() == "(" && (i-1) >= 0 && arExpression.at(i-1)->getType() == Token::TokenTypes::id) {
                argBracket = countBrackets;
                insideArgs = true;
            }
                // Arguments are present when there is : identifier[...
            else if (arExpression.at(i)->getLexeme() == "[" && (i-1) >= 0 && arExpression.at(i-1)->getType() == Token::TokenTypes::id) {
                argSqBracket = countSqBrackets;
                insideSqArgs = true;
            }
            else if (arExpression.at(i)->getLexeme() == ")") {
                if (countBrackets == (argBracket - 1)) {
                    insideArgs = false;
                }
            }
            else if (arExpression.at(i)->getLexeme() == "]") {
                if (countSqBrackets == (argSqBracket - 1)) {
                    insideSqArgs = false;
                }
            }

            // A variable which returns boolean can be used here.
            if (arExpression.at(i)->getType() == Token::TokenTypes::id && !insideArgs) {
                Symbol* currentSymbol = findSymbolNoError(arExpression.at(i)->getLexeme());

                if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                     currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") && currentSymbol->getType() == "int") {

                    intExpression = true;
                    break;
                }
            }

            if ((arExpression.at(i)->getLexeme() == "+" || arExpression.at(i)->getLexeme() == "-" || arExpression.at(i)->getLexeme() == "*" ||
                 arExpression.at(i)->getLexeme() == "/" || arExpression.at(i)->getType() == Token::TokenTypes::number) && !insideArgs) {

                intExpression = true;
                break;
            }
        }

        if (!intExpression) {
            for (int i = 0; i < sizeExpression; ++i) {
                if (arExpression.at(i)->getLexeme() == ".") {
                    if ((i-1) >= 0) {
                        Token *classToken = arExpression.at(i-1);

                        int classPosition = findClassPosition(classToken->getLexeme());

                        if ((i+1) < sizeExpression) {
                            Token *functionToken = arExpression.at(i+1);
                            Symbol *functionSymbol = findFunctionInClass(functionToken->getLexeme(), classPosition);

                            if (functionSymbol->getKind() == "not_found") {
                                cout << "Line: " << line << "\n";
                                cout << "Error: class " << classToken->getLexeme() << " does not have a definition for function "<< functionToken->getLexeme() <<".\n";
                                exit(EXIT_FAILURE);
                            }
                            else {
                                if (functionSymbol->getType() != "int") {
                                    cout << "Line: " << line << "\n";
                                    cout << "Error: in class " << classToken->getLexeme() << ", function "<< functionToken->getLexeme()
                                         <<" returns " << functionSymbol->getType() << "\n, however it should return int to resolve as an index for the array.\n";
                                    exit(EXIT_FAILURE);
                                }
                            }
                        }
                    }
                    intExpression = true;
                }
            }
        }

        // Array index does not return an integer.
        if (!intExpression) {
            cout << "Line: " << line << "\n";
            cout << "Error: the index of the array " << idName << " does not represent an integer value.\n";
            exit(EXIT_FAILURE);
        }

        nextToken = lexer->GetNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            ++line;
            
            nextToken = lexer->GetNextToken();
        }
        // End of file is reached.
        if (nextToken->getType() == Token::TokenTypes::eof) {
            cout << "Line: " << line << "\n";
            cout << "Error: unexpected end of file.\n";
            exit(EXIT_FAILURE);
        }

        if (nextToken->getLexeme() != "]") {
            cout << "Line: " << line << "\n";
            cout << "Error: expected ] at this position\n";
            exit(EXIT_FAILURE);
        }
    }

    typeLHS = idSymbol->getType();

    // Type compatibility is not checked for arrays
    if (hasArrayAccess) {
        typeLHS = "-1";
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "=") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected = at this position\n";
        exit(EXIT_FAILURE);
    }

    // Save the RHS until the semicolon or end of file. (Checks grammar later).

    int tokenNum = 0;

    // Vector of RHS tokens.
    vector<Token*> rhs;

    while (lexer->PeekNextTokenPlus(tokenNum)->getLexeme() != ";" &&
           lexer->PeekNextTokenPlus(tokenNum)->getType() != Token::TokenTypes::eof) {

        rhs.push_back(lexer->PeekNextTokenPlus(tokenNum));

        ++tokenNum;
    }

    // Checks that RHS is a valid expression.
    expression();

    bool insideArgs = false;
    bool insideSqArgs = false;

    // -1 => no need for type checking.
    if (typeLHS != "-1") {
        int size = rhs.size();

        for (int i = 0; i < size; ++i) {

            int countBrackets = 0;

            int countSqBrackets = 0;

            int argBracket = -1;
            int argSqBracket = -1;

            if (rhs.at(i)->getLexeme() == "(") {
                ++countBrackets;
            }
            else if (rhs.at(i)->getLexeme() == ")") {
                --countBrackets;
            }
            else if (rhs.at(i)->getLexeme() == "[") {
                ++countSqBrackets;
            }
            else if (rhs.at(i)->getLexeme() == "]") {
                --countSqBrackets;
            }

            // Arguments are present when there is : identifier(...
            if (rhs.at(i)->getLexeme() == "(" && (i-1) >= 0 && rhs.at(i-1)->getType() == Token::TokenTypes::id) {
                argBracket = countBrackets;
                insideArgs = true;
            }
                // Arguments are present when there is : identifier[...
            else if (rhs.at(i)->getLexeme() == "[" && (i-1) >= 0 && rhs.at(i-1)->getType() == Token::TokenTypes::id) {
                argSqBracket = countSqBrackets;
                insideSqArgs = true;
            }
            else if (rhs.at(i)->getLexeme() == ")") {
                if (countBrackets == (argBracket - 1)) {
                    insideArgs = false;
                }
            }
            else if (rhs.at(i)->getLexeme() == "]") {
                if (countSqBrackets == (argSqBracket - 1)) {
                    insideSqArgs = false;
                }
            }

            if (rhs.at(i)->getLexeme() == "null" && !insideArgs && !insideSqArgs) {
                typeRHS = "null";
                break;
            }

            // A variable which returns boolean can be used here.
            if (rhs.at(i)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                Symbol* currentSymbol = findSymbolNoError(rhs.at(i)->getLexeme());

                if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                    currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") && currentSymbol->getType() == "boolean") {

                    typeRHS = "boolean";
                    break;
                }
            }

            if ((rhs.at(i)->getLexeme() == "&" || rhs.at(i)->getLexeme() == "|" || rhs.at(i)->getLexeme() == "=" ||
                rhs.at(i)->getLexeme() == ">" || rhs.at(i)->getLexeme() == "<" || rhs.at(i)->getLexeme() == "~" ||
                rhs.at(i)->getLexeme() == "true" || rhs.at(i)->getLexeme() == "false") && !insideArgs && !insideSqArgs) {

                typeRHS = "boolean";
                break;
            }
        }

        insideArgs = false;
        insideSqArgs = false;

        // Still undetermined.
        if (typeRHS == "-1") {
            for (int i = 0; i < size; ++i) {

                int countBrackets = 0;

                int countSqBrackets = 0;

                int argBracket = -1;
                int argSqBracket = -1;

                if (rhs.at(i)->getLexeme() == "(") {
                    ++countBrackets;
                }
                else if (rhs.at(i)->getLexeme() == ")") {
                    --countBrackets;
                }
                else if (rhs.at(i)->getLexeme() == "[") {
                    ++countSqBrackets;
                }
                else if (rhs.at(i)->getLexeme() == "]") {
                    --countSqBrackets;
                }

                // Arguments are present when there is : identifier(...
                if (rhs.at(i)->getLexeme() == "(" && (i-1) >= 0 && rhs.at(i-1)->getType() == Token::TokenTypes::id) {
                    argBracket = countBrackets;
                    insideArgs = true;
                }
                    // Arguments are present when there is : identifier[...
                else if (rhs.at(i)->getLexeme() == "[" && (i-1) >= 0 && rhs.at(i-1)->getType() == Token::TokenTypes::id) {
                    argSqBracket = countSqBrackets;
                    insideSqArgs = true;
                }
                else if (rhs.at(i)->getLexeme() == ")") {
                    if (countBrackets == (argBracket - 1)) {
                        insideArgs = false;
                    }
                }
                else if (rhs.at(i)->getLexeme() == "]") {
                    if (countSqBrackets == (argSqBracket - 1)) {
                        insideSqArgs = false;
                    }
                }

                // A variable which returns boolean can be used here.
                if (rhs.at(i)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                    Symbol* currentSymbol = findSymbolNoError(rhs.at(i)->getLexeme());

                    if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                         currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") && currentSymbol->getType() == "int") {

                        typeRHS = "int";
                        break;
                    }
                }

                if ((rhs.at(i)->getLexeme() == "+" || rhs.at(i)->getLexeme() == "-" || rhs.at(i)->getLexeme() == "*" ||
                    rhs.at(i)->getLexeme() == "/" || rhs.at(i)->getType() == Token::TokenTypes::number) && !insideArgs && !insideSqArgs) {

                    typeRHS = "int";
                    break;
                }
            }
        }

        insideArgs = false;
        insideSqArgs = false;

        // And still undetermined.
        if (typeRHS == "-1") {
            for (int i = 0; i < size; ++i) {

                int countBrackets = 0;

                int countSqBrackets = 0;

                int argBracket = -1;
                int argSqBracket = -1;

                if (rhs.at(i)->getLexeme() == "(") {
                    ++countBrackets;
                }
                else if (rhs.at(i)->getLexeme() == ")") {
                    --countBrackets;
                }
                else if (rhs.at(i)->getLexeme() == "[") {
                    ++countSqBrackets;
                }
                else if (rhs.at(i)->getLexeme() == "]") {
                    --countSqBrackets;
                }

                // Arguments are present when there is : identifier(...
                if (rhs.at(i)->getLexeme() == "(" && (i-1) >= 0 && rhs.at(i-1)->getType() == Token::TokenTypes::id) {
                    argBracket = countBrackets;
                    insideArgs = true;
                }
                    // Arguments are present when there is : identifier[...
                else if (rhs.at(i)->getLexeme() == "[" && (i-1) >= 0 && rhs.at(i-1)->getType() == Token::TokenTypes::id) {
                    argSqBracket = countSqBrackets;
                    insideSqArgs = true;
                }
                else if (rhs.at(i)->getLexeme() == ")") {
                    if (countBrackets == (argBracket - 1)) {
                        insideArgs = false;
                    }
                }
                else if (rhs.at(i)->getLexeme() == "]") {
                    if (countSqBrackets == (argSqBracket - 1)) {
                        insideSqArgs = false;
                    }
                }

                // A variable which returns boolean can be used here.
                if (rhs.at(i)->getType() == Token::TokenTypes::id && !insideArgs && !insideSqArgs) {
                    Symbol* currentSymbol = findSymbolNoError(rhs.at(i)->getLexeme());

                    if ((currentSymbol->getKind() == "var" || currentSymbol->getKind() == "static" ||
                         currentSymbol->getKind() == "field" || currentSymbol->getKind() == "argument") && currentSymbol->getType() == "char") {

                        typeRHS = "char";
                        break;
                    }
                }

                if (rhs.at(i)->getType() == Token::TokenTypes::string_literal && !insideArgs && !insideSqArgs) {
                    if (rhs.at(i)->getLexeme().size() == 1 || rhs.at(i)->getLexeme().size() == 1) {
                        typeRHS = "char";
                    }
                    else {
                        typeRHS = "String";
                    }
                    break;
                }
            }
        }

        // Could be a user defined type.
        if (typeRHS == "-1") {
            bool checkForVar = true;

            for (int i = 0; i < size; ++i) {
                if (rhs.at(i)->getLexeme() == "." && ((i-1) >= 0) && ((i+1) < size)) {

                    checkForVar = false;
                    Symbol *classSymbol;
                    Symbol *funOrVarSymbol;

                    if (rhs.at(i-1)->getType() == Token::TokenTypes::id ||
                       (rhs.at(i-1)->getType() == Token::TokenTypes::keyword && rhs.at(i-1)->getLexeme() == "this")) {
                        classSymbol = findSymbolNoError(rhs.at(i - 1)->getLexeme());

                        if (classSymbol->getKind() == "class") {
                            // Return format: thisClass.function() or thisClass.var
                            if (classSymbol->getName() == className) {
                                funOrVarSymbol = findSymbolNoError((rhs.at(i + 1)->getLexeme()));

                                if (funOrVarSymbol->getType() != "not_found") {
                                    typeRHS = funOrVarSymbol->getType();
                                    break;
                                }
                                else {
                                    if (checkIfFunctionDeclared(rhs.at(i+1)->getLexeme())) {
                                        typeRHS = findUndeclaredFunctionType(rhs.at(i+1)->getLexeme());
                                    }
                                    else {
                                        cout << "Line: " << line << "\n";
                                        cout << "Error: use of undeclared function (" << rhs.at(i+1)->getLexeme() << ") in " << className << "class.\n";
                                        exit(EXIT_FAILURE);
                                    }
                                }
                            }
                            // Return format: otherClass.function() or otherClass.var
                            else {
                                if (classSymbol->getKind() != "not_found") {
                                    int classPos = findClassPosition(rhs.at(i-1)->getLexeme());

                                    Symbol *thisSymbol = findSymbolInClassNoError(rhs.at(i+1)->getLexeme(), classPos);

                                    if (thisSymbol->getType() != "not_found") {
                                        typeRHS = thisSymbol->getType();
                                        break;
                                    }
                                }
                            }
                        }
                            // Return format: varClass.function() or varClass.var
                        else if (classSymbol->getKind() == "var" || classSymbol->getKind() == "static" ||
                                 classSymbol->getKind() == "field") {

                            string varClassName = classSymbol->getType();
                            Symbol *varClassSymbol = findSymbolNoError(varClassName);

                            if (varClassName == className) {
                                funOrVarSymbol = findSymbolNoError((rhs.at(i + 1)->getLexeme()));
                                if (funOrVarSymbol->getType() != "not_found") {
                                    typeRHS = funOrVarSymbol->getType();
                                    break;
                                }
                            }
                                // Return format: otherClassVar.function() or otherClassVar.var
                            else {
                                if (varClassSymbol->getKind() != "not_found") {
                                    int classPos = findClassPosition(varClassName);

                                    Symbol *thisSymbol = findSymbolInClassNoError(rhs.at(i+1)->getLexeme(), classPos);

                                    if (thisSymbol->getType() != "not_found") {
                                        typeRHS = thisSymbol->getType();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (checkForVar) {
                for (int j = 0; j < size; ++j) {
                    if (rhs.at(j)->getType() == Token::TokenTypes::id) {
                        Symbol *idSymbol2 = findSymbolNoError(rhs.at(j)->getLexeme());

                        // Class type.
                        if (idSymbol2->getType() != "int" && idSymbol2->getType() != "char" &&
                            idSymbol2->getType() != "boolean") {

                            if (idSymbol2->getType() != "not_found") {
                                typeRHS = idSymbol2->getType();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    if (typeLHS != "-1" && typeLHS != typeRHS) {
        if ((typeLHS == "char" && typeRHS == "int") || (typeLHS == "int" && typeRHS == "char") || typeRHS == "null" ||
            (typeLHS == "int" && typeRHS == "boolean") || (typeLHS == "boolean" && typeRHS == "int") ||
            (typeLHS == "boolean" && typeRHS == "char") || (typeLHS == "char" && typeRHS == "boolean")) {
            // Char can be used as an integer.
            // Integer can be used as a char.
            // Null can be assigned to anything.
            // Boolean can be assigned to int and int can be assigned to boolean.
            // Boolean can be assigned to char and char can be assigned to boolean.
        }
        else {
            cout << "Line: " << line << "\n";
            cout << "Error: type of LHS (" << typeLHS << ") in let statement does not match the type of RHS ("
                 << typeRHS << ") in it.\n";
            exit(EXIT_FAILURE);
        }
    }


    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ";") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ';' before '" << nextToken->getLexeme() << "'\n";
        exit(EXIT_FAILURE);
    }

    // If not Array.
    if (!hasArrayAccess) {
        resolveExpression(rhs);
        if (identifierSymbol->getKind() == "var") {
            vmf << "pop local " << identifierSymbol->getPosition() << endl;
        }
        else if (identifierSymbol->getKind() == "field") {
            vmf << "pop this " << identifierSymbol->getPosition() << endl;
        }
        // Static or argument variable.
        else {
            vmf << "pop " << identifierSymbol->getKind() << " " << identifierSymbol->getPosition() << endl;
        }
    }
    else {
        resolveExpression(arExpression);

        if (identifierSymbol->getKind() == "var") {
            vmf << "push local " << identifierSymbol->getPosition() << endl;
        }
        else if (identifierSymbol->getKind() == "field") {
            vmf << "push this " << identifierSymbol->getPosition() << endl;
        }
        // Argument variable.
        else if (identifierSymbol->getKind() == "argument") {
            // If the current function is method, follow the symbol table positions for arguments.
            if (determineFunctionKind(determineFunctionPosition()) == "method") {
                vmf << "push " << identifierSymbol->getKind() << " " << identifierSymbol->getPosition() << endl;
            }
            // Else decrease the position of arguments by 1. (Because 'this' takes up the first position).
            else {
                vmf << "push " << identifierSymbol->getKind() << " " << identifierSymbol->getPosition() - 1 << endl;
            }
        }
        // Static variable.
        else  {
            vmf << "push " << identifierSymbol->getKind() << " " << identifierSymbol->getPosition() << endl;
        }

        vmf << "add" << endl;

        vmf << "pop pointer 1" << endl;

        resolveExpression(rhs);

        vmf << "pop that 0" << endl;
    }

    // Now the variable is initialised.
    idSymbol->setInitialised();
}

// Statement -> VarDeclarStatement | LetStatemnt | IfStatement | WhileStatement | DoStatement | ReturnStatemnt
void Parser::statement() {
    Token *nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    if (nextToken->getLexeme() == "var") {
        varDeclarStatement();
    }
    else if (nextToken->getLexeme() == "let") {
        letStatement();
    }
    else if (nextToken->getLexeme() == "if") {
        ifStatement();
    }
    else if (nextToken->getLexeme() == "while") {
        whileStatement();
    }
    else if (nextToken->getLexeme() == "do") {
        doStatement();
    }
    else if (nextToken->getLexeme() == "return") {
        returnStatement();

        functionHasReturn = 1;
    }
    else {
        cout << "Line: " << line << "\n";
        cout << "Error: invalid statement\n";
        exit(EXIT_FAILURE);
    }
}

// SubroutineBody -> { {Statement} }
void Parser::subroutineBody() {
    Token *nextToken = lexer->GetNextToken();

    bool emptyBody = true;

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {

        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "{") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected { at this position\n";
        exit(EXIT_FAILURE);
    }

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    // Doesn't have a return initially (before checked).
    functionHasReturn = 0;

    // Doesn't return in all paths initially (before checked).
    returnInAllPaths = -1;

    // Meaningful if statement is the one which is declared in the main scope of the current function.
    inMeaningfulIf = 0;

    int position = determineFunctionPosition();

    // 0 or more statements are expected now.
    while (nextToken->getLexeme() == "var" || nextToken->getLexeme() == "let" || nextToken->getLexeme() == "if" ||
           nextToken->getLexeme() == "while" || nextToken->getLexeme() == "do" || nextToken->getLexeme() == "return") {

        if (nextToken->getLexeme() == "if") {
            if (returnInAllPaths != 2 && returnInAllPaths != 1) {
                returnInAllPaths = -1;
            }
        }

        if (returnInAllPaths == 1 || returnInAllPaths == 2) {
            cout << "Line: " << line << "\n";
            cout << "Warning: unreachable code in function (" << determineFunctionName(position) << ") is present.\n";
        }

        emptyBody = false;

        statement();

        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }

    string functionType = determineFunctionType(position);

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "}") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected } at this position\n";
        exit(EXIT_FAILURE);
    }

    // If the function has an empty body, then it is a header function. Do not check anything.
    // If there is some return statement, then it should be "return;" -> void, which is checked in returnStatement.
    // Otherwise there must be no return statement.
    if (!emptyBody) {
        if (functionType != "void") {
            // No return was found, however function is non-void.
            if (functionHasReturn == 0) {
                cout << "Line: " << line << "\n";
                cout << "Error: non-void function (" << determineFunctionName(position) << ") has a void return.\n";
                exit(EXIT_FAILURE);
            }

            if (returnInAllPaths == 0) {
                cout << "Line: " << line << "\n";
                cout << "Warning: not all meaningful paths of the non-void function (" << determineFunctionName(position) <<
                        ") have a return statement associated with it.\n";
            }
        }
    }
    // After the definition of a function, the scope becomes a class scope (scope 1).
    currentTable = 1;

    // Meaning it's not checked.
    functionHasReturn = -1;

    returnInAllPaths = -1;

    inMeaningfulIf = -1;

}

// SubroutineDeclar -> (constructor | function | method) (Type|void) identifier (ParamList) SubroutineBody
void Parser::subroutineDeclar() {
    Token *nextToken = lexer->GetNextToken();

    hasReturn = false;

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "constructor" && nextToken->getLexeme() != "function" &&
        nextToken->getLexeme() != "method") {

        cout << "Line: " << line << "\n";
        cout << "Error: expected a subroutine declaration at this position\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() == "constructor") {
        inConstructor = true;
    }

    string kindV = nextToken->getLexeme();

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    string typeV;

    if (nextToken->getLexeme() == "void") {
        // Consume void.
        nextToken = lexer->GetNextToken();

        typeV = nextToken->getLexeme();
    }
    else {
        nextToken = lexer->PeekNextToken();

        typeV = nextToken->getLexeme();

        type();
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getType() != Token::TokenTypes::id) {
        cout << "Line: " << line << "\n";
        cout << "Error: expected an identifier at this position\n";
        exit(EXIT_FAILURE);
    }

    string name = nextToken->getLexeme();

    int numberOfLocalVars = 0;
    int bracketsCount = 0;
    int countToken = 0;

    bool inLine = false;

    Token *tempToken = lexer->PeekNextTokenPlus(countToken);

    while (true) {

        if (tempToken->getLexeme() == "{") {
            ++bracketsCount;
        }
        else if (tempToken->getLexeme() == "}") {
            --bracketsCount;
            if (bracketsCount == 0) {
                // Finished copying.
                break;
            }
        }
        else if (tempToken->getLexeme() == ",") {
            if (inLine) {
                ++numberOfLocalVars;
            }
        }
        else if (tempToken->getLexeme() == ";") {
            inLine = false;
        }
        else if (tempToken->getType() == Token::TokenTypes::eof) {
            break;
        }
        else if (tempToken->getLexeme() == "var") {
            ++numberOfLocalVars;
            inLine = true;
        }
        ++countToken;
        tempToken = lexer->PeekNextTokenPlus(countToken);
    }

    vmf << "function " << className << "." << name << " " << numberOfLocalVars << endl;

    if (inConstructor) {
        // Reserving memory for the class variables.
        vmf << "push constant " << numberOfClassVars << endl;
        vmf << "call Memory.alloc 1" << endl;

        // Popping the memory to 'this'.
        vmf << "pop pointer 0" << endl;
    }

    searchForRedeclaration(name);
    addToTable(name, typeV, kindV);

    currentFunctionSymbol = findSymbol(name);

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "(") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ( at this position\n";
        exit(EXIT_FAILURE);
    }

    // A new function must be declared only in the scope of the class (Scope 1).
    currentProgramState.at(thisClassPosition).emplace_back();
    currentTable = (int) currentProgramState.at(thisClassPosition).size() - 1;

    addToTable("this", className, "argument");


    if (!inConstructor) {
        // Pushing "this" to the corresponding "this" segment;
        if (kindV != "function") {
            vmf << "push argument 0" << endl;
            vmf << "pop pointer 0" << endl;
        }
    }

    paramList();

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != ")") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected ) at this position\n";
        exit(EXIT_FAILURE);
    }

    subroutineBody();

    currentFunctionSymbol = nullptr;

    if (!hasReturn && !inSubroutineCall) {
        vmf << "return" << endl;
    }

    hasReturn = false;
    inConstructor = false;
}

// MemberDeclar -> ClassVarDeclar | SubroutineDeclar
void Parser::memberDeclar() {
    Token *nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    if (nextToken->getLexeme() == "static" || nextToken->getLexeme() == "field") {
        classVarDeclar();
    }
    else if (nextToken->getLexeme() == "constructor" || nextToken->getLexeme() == "function" ||
             nextToken->getLexeme() == "method") {

        subroutineDeclar();
    }
    else {
        cout << "Line: " << line << "\n";
        cout << "Error: invalid member declaration.\n";
        exit(EXIT_FAILURE);
    }
}

// ClassDeclar -> class identifier { {MemberDeclar} }
void Parser::classDeclar() {
    Token *nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "class") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected class at this position\n";
        exit(EXIT_FAILURE);
    }

    int bracketsCount = 0;
    int countToken = 0;

    bool inLine = false;

    Token *tempToken = lexer->PeekNextTokenPlus(countToken);

    while (true) {

        if (tempToken->getLexeme() == "{") {
            ++bracketsCount;
        }
        else if (tempToken->getLexeme() == "}") {
            --bracketsCount;
            if (bracketsCount == 0) {
                // Finished copying.
                break;
            }
        }
        else if (inLine && tempToken->getLexeme() == ",") {
            ++numberOfClassVars;
        }
        else if (tempToken->getLexeme() == ";") {
            inLine = false;
        }
        else if (tempToken->getType() == Token::TokenTypes::eof) {
            break;
        }
        else if ((tempToken->getLexeme() == "field" || tempToken->getLexeme() == "static") && tempToken->getType() == Token::TokenTypes::keyword) {
            ++numberOfClassVars;
            inLine = true;
        }
        ++countToken;
        tempToken = lexer->PeekNextTokenPlus(countToken);
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getType() != Token::TokenTypes::id) {
        cout << "Line: " << line << "\n";
        cout << "Error: expected an identifier at this position\n";
        exit(EXIT_FAILURE);
    }

    className = nextToken->getLexeme();

    // If the class name does not match the file name, throw a warning. ie: Big.jack has class small.
    if (className != fileClassName) {
        cout << "Line: " << line << "\n";
        cout << "Warning: class name (" << className << ") should match the file name (" << fileClassName << ").\n";
    }

    // Makes sure that every class has a different class name.
    searchForRedeclaration(className);

    addToTable(className, className, "class");

    // Meaningful symbol showing that this global table belongs to that class in the whole program.
    addToTable(className, className, className);

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "{") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected { at this position\n";
        exit(EXIT_FAILURE);
    }

    // New scope.
    currentProgramState.at(thisClassPosition).emplace_back();
    ++currentTable;

    nextToken = lexer->PeekNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        // Consume new_line.
        nextToken = lexer->GetNextToken();

        ++line;

        // Check if new_line appears again.
        nextToken = lexer->PeekNextToken();
    }

    while (nextToken->getLexeme() == "constructor" || nextToken->getLexeme() == "function" ||
           nextToken->getLexeme() == "method" || nextToken->getLexeme() == "static" ||
           nextToken->getLexeme() == "field") {

        memberDeclar();

        nextToken = lexer->PeekNextToken();

        // Consuming new lines and increasing the line counter for correct error display.
        while (nextToken->getType() == Token::TokenTypes::new_line) {
            // Consume new_line.
            nextToken = lexer->GetNextToken();

            ++line;

            // Check if new_line appears again.
            nextToken = lexer->PeekNextToken();
        }
    }

    nextToken = lexer->GetNextToken();

    // Consuming new lines and increasing the line counter for correct error display.
    while (nextToken->getType() == Token::TokenTypes::new_line) {
        ++line;
        
        nextToken = lexer->GetNextToken();
    }
    // End of file is reached.
    if (nextToken->getType() == Token::TokenTypes::eof) {
        cout << "Line: " << line << "\n";
        cout << "Error: unexpected end of file.\n";
        exit(EXIT_FAILURE);
    }

    if (nextToken->getLexeme() != "}") {
        cout << "Line: " << line << "\n";
        cout << "Error: expected } at this position\n";
        exit(EXIT_FAILURE);
    }

    // Global table.
    currentTable = 0;

    // Closes the vm file.
    vmf.close();
}




