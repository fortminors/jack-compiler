//
// Created by sc18gg on 2/8/20.
//

/**
 * This file contains all the functions used by and for the Tokens in the Jack file.
 */

#include <iostream>

using namespace std;

#include "Token.h"

const string &Token::getLexeme() const {
    return lexeme;
}

void Token::setLexeme(const string &lexeme) {
    Token::lexeme = lexeme;
}

Token::TokenTypes Token::getType() const {
    return type;
}

void Token::setType(Token::TokenTypes type) {
    Token::type = type;
}

Token::Token(const string &lexeme, Token::TokenTypes type) : lexeme(lexeme), type(type){}

int Token::getLineNumber() const {
    if (this->getType() == id) {
        return lineNumber;
    }
    else {
        cout << "Error, unable to extract the line number as the token is not an identifier\n";
        exit(EXIT_FAILURE);
    }
}

void Token::setLineNumber(int line) {
    Token::lineNumber = line;
}
